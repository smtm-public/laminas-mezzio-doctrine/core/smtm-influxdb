<?php

declare(strict_types=1);

namespace Smtm\InfluxDB;

use Smtm\Base\Infrastructure\Helper\EnvHelper;

if (file_exists(__DIR__ . '/../../../../.env.smtm.smtm-influxdb')) {
    $dotenv = \Dotenv\Dotenv::createMutable(__DIR__ . '/../../../../', '.env.smtm.smtm-influxdb');
    $dotenv->load();
}

return [
    'services' => json_decode(
        EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_INFLUXDB_SERVICES'),
        true,
        flags: JSON_THROW_ON_ERROR
    ),
];
