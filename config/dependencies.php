<?php

declare(strict_types=1);

namespace Smtm\InfluxDB;

use Smtm\Base\Application\Service\ApplicationServicePluginManager;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Smtm\InfluxDB\Infrastructure\Service\RemoteServiceConnector\Factory\InfluxDBConnectorServiceFactory;
use Smtm\InfluxDB\Infrastructure\Service\RemoteServiceConnector\InfluxDBConnectorService;
use Psr\Container\ContainerInterface;

return [
    'delegators' => [
        InfrastructureServicePluginManager::class => [
            function (
                ContainerInterface $container,
                $name,
                callable $callback,
                array $options = null
            ) {
                /** @var InfrastructureServicePluginManager $infrastructureServicePluginManager */
                $infrastructureServicePluginManager = $callback();

                $infrastructureServicePluginManager->setFactory(
                    InfluxDBConnectorService::class,
                    InfluxDBConnectorServiceFactory::class
                );

                foreach ($container->get('config')['influxdb']['services'] ?? [] as $influxdbServiceName => $influxdbServiceConfig) {
                    $infrastructureServicePluginManager->setFactory(
                        $influxdbServiceName,
                        function (
                            ContainerInterface $container,
                            $requestedName,
                            ?array $options = null
                        ) use ($influxdbServiceConfig) {
                            /** @var InfrastructureServicePluginManager $infrastructureServicePluginManager */
                            $infrastructureServicePluginManager = $container->get(
                                InfrastructureServicePluginManager::class
                            );

                            if ($options !== null) {
                                $influxdbServiceConfig = array_replace_recursive(
                                    $influxdbServiceConfig,
                                    $options
                                );
                            }

                            return $infrastructureServicePluginManager->build(
                                InfluxDBConnectorService::class,
                                $influxdbServiceConfig
                            );
                        }
                    );
                }

                return $infrastructureServicePluginManager;
            }
        ],
    ],
];
