<?php

declare(strict_types=1);

namespace Smtm\InfluxDB\Factory;

use Smtm\Base\Factory\ConfigAwareDelegator;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class InfluxDBConfigAwareDelegator extends ConfigAwareDelegator
{
    protected array|string|null $configKey = ['influxdb'];
}
