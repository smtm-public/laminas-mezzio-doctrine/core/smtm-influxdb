<?php

declare(strict_types=1);

namespace Smtm\InfluxDB;

use JetBrains\PhpStorm\ArrayShape;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ConfigProvider
{
    #[ArrayShape([
        'dependencies' => 'array',
        'influxdb' => 'array',
    ])] public function __invoke(): array
    {
        return [
            'dependencies' => include __DIR__ . '/../config/dependencies.php',
            'influxdb' => include __DIR__ . '/../config/influxdb.php',
        ];
    }
}
