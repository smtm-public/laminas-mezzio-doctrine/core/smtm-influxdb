<?php

declare(strict_types=1);

namespace Smtm\InfluxDB\Infrastructure;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface RepositoryAwareInterface
{
    public function getRepository(): AbstractRepository;
    public function setRepository(AbstractRepository $repository): static;
}
