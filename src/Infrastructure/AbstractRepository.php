<?php

declare(strict_types=1);

namespace Smtm\InfluxDB\Infrastructure;

use Smtm\Base\ConfigCollectionAwareInterface;
use Smtm\Base\ConfigCollectionAwareTrait;
use Smtm\Base\Infrastructure\Repository\RepositoryInterface as BaseRepositoryInterface;
use Smtm\Base\Infrastructure\Service\AbstractInfrastructureService;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Smtm\Base\Infrastructure\Service\Log\LoggerCollectionAwareInterface;
use Smtm\Base\Infrastructure\Service\Log\LoggerCollectionAwareTrait;
use Smtm\InfluxDB\Infrastructure\Helper\InfluxDBHelper;
use Smtm\InfluxDB\Infrastructure\QueryBuilder\Func\AggregateWindow;
use Smtm\InfluxDB\Infrastructure\Service\RemoteServiceConnector\InfluxDBConnectorService;
use JetBrains\PhpStorm\ArrayShape;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class AbstractRepository extends AbstractInfrastructureService implements RepositoryInterface,
    ConfigCollectionAwareInterface,
    LoggerCollectionAwareInterface
{

    use ConfigCollectionAwareTrait, LoggerCollectionAwareTrait;

    public function __construct(
        protected InfrastructureServicePluginManager $infrastructureServicePluginManager,
        protected InfluxDBConnectorService $connectorService,
        protected string $bucket,
        protected array $config = [
            RepositoryInterface::CONFIG_KEY_LOGGING_ENABLED => false,
        ]
    ) {
        parent::__construct($this->infrastructureServicePluginManager);

        $this->config[RepositoryInterface::CONFIG_KEY_LOGGING_ENABLED] =
            $config[RepositoryInterface::CONFIG_KEY_LOGGING_ENABLED] ?? false;
    }

    /**
     * THE DATE PERIOD CRITERION MUST BE THE FIRST ELEMENT IN THE CRITERIA ARRAY
     */
    public function findBy(
        array $criteria,
        ?array $orderBy = null,
        ?int $limit = null,
        ?int $offset = null,
        #[ArrayShape([
            BaseRepositoryInterface::OPTION_KEY_CACHE_STORAGE => 'string',
            BaseRepositoryInterface::OPTION_KEY_HYDRATION_MODE => 'int | null',
            BaseRepositoryInterface::OPTION_KEY_RESULT_COLLECTION_INDEX_BY => 'string | null',
            RepositoryInterface::OPTION_KEY_AGGREGATE => 'bool',
            RepositoryInterface::OPTION_KEY_CREATE_EMPTY => 'bool',
            RepositoryInterface::OPTION_KEY_USE_PREVIOUS => 'bool',
            RepositoryInterface::OPTION_KEY_PIVOT => 'array',
            RepositoryInterface::OPTION_KEY_SKIP_SORTING => 'bool',
            RepositoryInterface::OPTION_KEY_PARSER_OPTIONS => ParserOptions::class . '|array',
            RepositoryInterface::OPTION_KEY_CUSTOM_GROUP => 'array',
            RepositoryInterface::OPTION_KEY_KEEP => 'array',
            RepositoryInterface::OPTION_KEY_LOG_QUERY => 'bool | null',
            RepositoryInterface::OPTION_KEY_IMPORT_COLLECTION => 'array',
            RepositoryInterface::OPTION_KEY_MAP => 'array',
            RepositoryInterface::OPTION_KEY_RENAME => 'array',
            RepositoryInterface::OPTION_KEY_TO_STRING => 'bool | null',
        ])] array $options = []
    ): array|\Generator {
        $query = $this->buildQuery(
            $criteria,
            $orderBy,
            $limit,
            $offset,
            $options
        );
        $parserOptions = array_key_exists(RepositoryInterface::OPTION_KEY_PARSER_OPTIONS, $options)
            ? $options[RepositoryInterface::OPTION_KEY_PARSER_OPTIONS]
            : [];

        return $this->connectorService->query($query, $parserOptions);
    }

    public function buildQuery(
        array $criteria,
        ?array $orderBy = null,
        ?int $limit = null,
        ?int $offset = null,
        #[ArrayShape([
            BaseRepositoryInterface::OPTION_KEY_CACHE_STORAGE => 'string',
            BaseRepositoryInterface::OPTION_KEY_HYDRATION_MODE => 'int | null',
            BaseRepositoryInterface::OPTION_KEY_RESULT_COLLECTION_INDEX_BY => 'string | null',
            RepositoryInterface::OPTION_KEY_AGGREGATE => 'bool',
            RepositoryInterface::OPTION_KEY_CREATE_EMPTY => 'bool',
            RepositoryInterface::OPTION_KEY_USE_PREVIOUS => 'bool',
            RepositoryInterface::OPTION_KEY_PIVOT => 'array',
            RepositoryInterface::OPTION_KEY_SKIP_SORTING => 'bool',
            RepositoryInterface::OPTION_KEY_PARSER_OPTIONS => ParserOptions::class . '|array',
            RepositoryInterface::OPTION_KEY_CUSTOM_GROUP => 'array',
            RepositoryInterface::OPTION_KEY_KEEP => 'array',
            RepositoryInterface::OPTION_KEY_LOG_QUERY => 'bool | null',
            RepositoryInterface::OPTION_KEY_IMPORT_COLLECTION => 'array',
            RepositoryInterface::OPTION_KEY_MAP => 'array',
            RepositoryInterface::OPTION_KEY_RENAME => 'array',
            RepositoryInterface::OPTION_KEY_TO_STRING => 'bool | null',
        ])] array $options = []
    ): string {
        $queryBuilder = new QueryBuilder();
        $queryBuilder->addFrom($this->bucket);

        $filters = InfluxDBHelper::resolveCriteria(
            $queryBuilder,
            $criteria,
            [
                'usageTypeCode' => '_measurement',
                'usageType.code' => '_measurement',
                'contract.number' => 'contract_number',
                'contractNumber' => 'contract_number',
                'account.number' => 'account_number',
                'accountNumber' => 'account_number',
                'site.number' => 'site_number',
                'siteNumber' => 'site_number',
                'gridOperator.code' => 'grid_operator',
                'gridOperatorCode' => 'grid_operator',
                'gridTypeCode' => 'grid_type',
                'gridType.code' => 'grid_type'
            ]
        );

        if (!empty($filters)) {
            $queryBuilder->push($filters);
        }

        $datePeriod = InfluxDBHelper::getDatePeriodFromCriteria($criteria, 'datePeriod');
        $pivot = $options[RepositoryInterface::OPTION_KEY_PIVOT] ?? null;

        if (!empty($options[RepositoryInterface::OPTION_KEY_AGGREGATE])) {
            $queryBuilder->addAggregation(
                $datePeriod,
                AggregateWindow::FN_SUM,
                $options[RepositoryInterface::OPTION_KEY_CREATE_EMPTY] ?? false,
                $options[RepositoryInterface::OPTION_KEY_USE_PREVIOUS] ?? false
            );
        }

        if (!empty($pivot) && isset($pivot['addBeforeGroup']) && $pivot['addBeforeGroup']) {
            $queryBuilder->addPivot($pivot['valueColumn'], $pivot['rowKeys'], $pivot['columnKeys']);
        }

        if (!isset($options[RepositoryInterface::OPTION_KEY_CUSTOM_GROUP])) {
            $queryBuilder->addGroup();
        } else {
            $groupOptions = $options[RepositoryInterface::OPTION_KEY_CUSTOM_GROUP];
            $columns = $groupOptions['columns'] ?? null;
            $queryBuilder->addGroup($columns);
        }

        if (!empty($pivot) && (!isset($pivot['addBeforeGroup']) || !$pivot['addBeforeGroup'])) {
            $queryBuilder->addPivot($pivot['valueColumn'], $pivot['rowKeys'], $pivot['columnKeys']);
        }

        if (!empty($options[RepositoryInterface::OPTION_KEY_MAP])) {
            $mapOptions = $options[RepositoryInterface::OPTION_KEY_MAP];

            $queryBuilder->addMap(
                $mapOptions['with'],
                $mapOptions['outputRecord'],
                $mapOptions['scopedVariables'] ?? null
            );
        }

        if ((!array_key_exists(RepositoryInterface::OPTION_KEY_SKIP_SORTING, $options)
            || !$options[RepositoryInterface::OPTION_KEY_SKIP_SORTING]) && !empty($orderBy)
        ) {
            if (empty(array_intersect($orderBy, [BaseRepositoryInterface::ORDER_ASC, BaseRepositoryInterface::ORDER_DESC]))) {
                $queryBuilder->addSort($orderBy);
            } else {
                foreach ($orderBy as $field => $order) {
                    $queryBuilder->addSort(
                        [$field] ?? [],
                        !($order === BaseRepositoryInterface::ORDER_ASC)
                    );
                }
            }
        }

        if ($limit !== null && $offset !== null) {
            $queryBuilder->addLimit($limit, $offset);
        }

        if (array_key_exists(RepositoryInterface::OPTION_KEY_KEEP, $options)) {
            $queryBuilder->addKeep($options[RepositoryInterface::OPTION_KEY_KEEP]);
        }

        if (array_key_exists(RepositoryInterface::OPTION_KEY_RENAME, $options)) {
            $rename = $options[RepositoryInterface::OPTION_KEY_RENAME];
            $queryBuilder->addRename(
                $rename['columns'] ?? null,
                $rename['newColumnName'] ?? null,
                $rename['condition'] ?? null
            );
        }

        if (isset($options[RepositoryInterface::OPTION_KEY_TO_STRING])
            && $options[RepositoryInterface::OPTION_KEY_TO_STRING]
        ) {
            $queryBuilder->addToString();
        }

        $queryBuilder->addYield();
        $query = $queryBuilder->__toString();
        $query = InfluxDBHelper::addImports(
            $query,
            $options[RepositoryInterface::OPTION_KEY_IMPORT_COLLECTION] ?? [],
            $datePeriod,
            $options[RepositoryInterface::OPTION_KEY_AGGREGATE] ?? false
        );
        ($options[RepositoryInterface::OPTION_KEY_LOG_QUERY] ?? $this->config[RepositoryInterface::CONFIG_KEY_LOGGING_ENABLED])
            && $this->getLoggerByName('full')
            && $this->getLoggerByName('full')->info("\n" . $query . "\n\n\n");

        return $query;
    }

    public function findCountBy(
        array $criteria,
        #[ArrayShape([
            BaseRepositoryInterface::OPTION_KEY_CACHE_STORAGE => 'string',
            BaseRepositoryInterface::OPTION_KEY_HYDRATION_MODE => 'int | null',
            BaseRepositoryInterface::OPTION_KEY_RESULT_COLLECTION_INDEX_BY => 'string | null',
            RepositoryInterface::OPTION_KEY_AGGREGATE => 'bool',
            RepositoryInterface::OPTION_KEY_CREATE_EMPTY => 'bool',
            RepositoryInterface::OPTION_KEY_USE_PREVIOUS => 'bool',
            RepositoryInterface::OPTION_KEY_PIVOT => 'array',
            RepositoryInterface::OPTION_KEY_SKIP_SORTING => 'bool',
            RepositoryInterface::OPTION_KEY_PARSER_OPTIONS => ParserOptions::class . '|array',
            RepositoryInterface::OPTION_KEY_CUSTOM_GROUP => 'array',
            RepositoryInterface::OPTION_KEY_KEEP => 'array',
            RepositoryInterface::OPTION_KEY_COUNT_COLUMN => 'string',
            RepositoryInterface::OPTION_KEY_LOG_QUERY => 'bool | null',
        ])] array $options = []
    ): int {
        $queryBuilder = new QueryBuilder();
        $queryBuilder->addFrom($this->bucket);

        $filters = InfluxDBHelper::resolveCriteria(
            $queryBuilder,
            $criteria,
            [
                'usageTypeCode' => '_measurement',
                'usageType.code' => '_measurement',
                'contract.number' => 'contract_number',
                'contractNumber' => 'contract_number',
                'account.number' => 'account_number',
                'accountNumber' => 'account_number',
                'site.number' => 'site_number',
                'siteNumber' => 'site_number',
                'gridOperator.code' => 'grid_operator',
                'gridOperatorCode' => 'grid_operator',
                'gridTypeCode' => 'grid_type',
                'gridType.code' => 'grid_type'
            ]
        );

        if (!empty($filters)) {
            $queryBuilder->push($filters);
        }

        $datePeriod = InfluxDBHelper::getDatePeriodFromCriteria($criteria, 'datePeriod');
        $countColumn = $options[RepositoryInterface::OPTION_KEY_COUNT_COLUMN] ?? null;

        if (!empty($options[RepositoryInterface::OPTION_KEY_AGGREGATE])) {
            $queryBuilder->addAggregation(
                $datePeriod,
                AggregateWindow::FN_SUM,
                $options[RepositoryInterface::OPTION_KEY_CREATE_EMPTY] ?? false,
                $options[RepositoryInterface::OPTION_KEY_USE_PREVIOUS] ?? false
            );
        }

        $queryBuilder->addGroup();
        $queryBuilder->addCount($countColumn);
        $queryBuilder->addYield();

        $query = $queryBuilder->__toString();
        $query = InfluxDBHelper::addImports(
            $query,
            datePeriod: $datePeriod,
            aggregation: $options[RepositoryInterface::OPTION_KEY_AGGREGATE] ?? false
        );
        ($options[RepositoryInterface::OPTION_KEY_LOG_QUERY] ?? $this->config[RepositoryInterface::CONFIG_KEY_LOGGING_ENABLED])
            && $this->getLoggerByName('full')
            && $this->getLoggerByName('full')->info("\n" . $query . "\n\n\n");

        $result = $this->connectorService->query($query);

        return $result[0][$countColumn ?? '_value'] ?? 0;
    }

    public function findByWithCount(
        array $criteria,
        ?array $orderBy = null,
        ?int $limit = null,
        ?int $offset = null,
        #[ArrayShape([
            BaseRepositoryInterface::OPTION_KEY_CACHE_STORAGE => 'string',
            BaseRepositoryInterface::OPTION_KEY_HYDRATION_MODE => 'int | null',
            BaseRepositoryInterface::OPTION_KEY_RESULT_COLLECTION_INDEX_BY => 'string | null',
            RepositoryInterface::OPTION_KEY_AGGREGATE => 'bool',
            RepositoryInterface::OPTION_KEY_CREATE_EMPTY => 'bool',
            RepositoryInterface::OPTION_KEY_USE_PREVIOUS => 'bool',
            RepositoryInterface::OPTION_KEY_PIVOT => 'array',
            RepositoryInterface::OPTION_KEY_SKIP_SORTING => 'bool',
            RepositoryInterface::OPTION_KEY_PARSER_OPTIONS => ParserOptions::class . '|array',
            RepositoryInterface::OPTION_KEY_CUSTOM_GROUP => 'array',
            RepositoryInterface::OPTION_KEY_KEEP => 'array',
            RepositoryInterface::OPTION_KEY_COUNT_COLUMN => 'string',
            RepositoryInterface::OPTION_KEY_LOG_QUERY => 'bool | null',
            RepositoryInterface::OPTION_KEY_IMPORT_COLLECTION => 'array',
            RepositoryInterface::OPTION_KEY_MAP => 'array',
            RepositoryInterface::OPTION_KEY_RENAME => 'array',
            RepositoryInterface::OPTION_KEY_TO_STRING => 'bool | null',
        ])] array $options = []
    ): array {
        $queryBuilder = new QueryBuilder();
        $queryBuilder->addFrom($this->bucket);

        $filters = InfluxDBHelper::resolveCriteria(
            $queryBuilder,
            $criteria,
            [
                'usageTypeCode' => '_measurement',
                'usageType.code' => '_measurement',
                'contract.number' => 'contract_number',
                'contractNumber' => 'contract_number',
                'account.number' => 'account_number',
                'accountNumber' => 'account_number',
                'site.number' => 'site_number',
                'siteNumber' => 'site_number',
                'gridOperator.code' => 'grid_operator',
                'gridOperatorCode' => 'grid_operator',
                'gridTypeCode' => 'grid_type',
                'gridType.code' => 'grid_type'
            ]
        );

        if (!empty($filters)) {
            $queryBuilder->push($filters);
        }

        $datePeriod = InfluxDBHelper::getDatePeriodFromCriteria($criteria, 'datePeriod');
        $pivot = $options[RepositoryInterface::OPTION_KEY_PIVOT] ?? null;

        if (!empty($options[RepositoryInterface::OPTION_KEY_AGGREGATE])) {
            $queryBuilder->addAggregation(
                $datePeriod,
                AggregateWindow::FN_SUM,
                $options[RepositoryInterface::OPTION_KEY_CREATE_EMPTY] ?? false,
                $options[RepositoryInterface::OPTION_KEY_USE_PREVIOUS] ?? false
            );
        }

        if (!empty($pivot) && isset($pivot['addBeforeGroup']) && $pivot['addBeforeGroup']) {
            $queryBuilder->addPivot($pivot['valueColumn'], $pivot['rowKeys'], $pivot['columnKeys']);
        }

        if (!isset($options[RepositoryInterface::OPTION_KEY_CUSTOM_GROUP])) {
            $queryBuilder->addGroup();
        } else {
            $groupOptions = $options[RepositoryInterface::OPTION_KEY_CUSTOM_GROUP];
            $columns = $groupOptions['columns'] ?? null;
            $queryBuilder->addGroup($columns);
        }

        if (!empty($pivot) && (!isset($pivot['addBeforeGroup']) || !$pivot['addBeforeGroup'])) {
            $queryBuilder->addPivot($pivot['valueColumn'], $pivot['rowKeys'], $pivot['columnKeys']);
        }

        if (!empty($options[RepositoryInterface::OPTION_KEY_MAP])) {
            $mapOptions = $options[RepositoryInterface::OPTION_KEY_MAP];

            $queryBuilder->addMap(
                $mapOptions['with'],
                $mapOptions['outputRecord'],
                $mapOptions['scopedVariables'] ?? null
            );
        }

        $countQueryBuilder = clone $queryBuilder;

        if ((!array_key_exists(RepositoryInterface::OPTION_KEY_SKIP_SORTING, $options)
            || !$options[RepositoryInterface::OPTION_KEY_SKIP_SORTING]) && !empty($orderBy)
        ) {
            if (empty(array_intersect($orderBy, [BaseRepositoryInterface::ORDER_ASC, BaseRepositoryInterface::ORDER_DESC]))) {
                $queryBuilder->addSort($orderBy);
            } else {
                foreach ($orderBy as $field => $order) {
                    $queryBuilder->addSort(
                        [$field] ?? [],
                        !($order === BaseRepositoryInterface::ORDER_ASC)
                    );
                }
            }
        }

        if ($limit !== null && $offset !== null) {
            $queryBuilder->addLimit($limit, $offset);
        }

        if (array_key_exists(RepositoryInterface::OPTION_KEY_KEEP, $options)) {
            $queryBuilder->addKeep($options[RepositoryInterface::OPTION_KEY_KEEP]);
        }

        if (array_key_exists(RepositoryInterface::OPTION_KEY_RENAME, $options)) {
            $rename = $options[RepositoryInterface::OPTION_KEY_RENAME];
            $queryBuilder->addRename(
                $rename['columns'] ?? null,
                $rename['newColumnName'] ?? null,
                $rename['condition'] ?? null
            );
        }

        if (isset($options[RepositoryInterface::OPTION_KEY_TO_STRING])
            && $options[RepositoryInterface::OPTION_KEY_TO_STRING]
        ) {
            $queryBuilder->addToString();
        }

        if (array_key_exists(RepositoryInterface::OPTION_KEY_COUNT_COLUMN, $options)) {
            $countColumn = $options[RepositoryInterface::OPTION_KEY_COUNT_COLUMN];
            $countQueryBuilder->addCount($countColumn);
        } else {
            $countQueryBuilder->addCount();
        }

        $countQueryBuilder->addYield();
        $countQuery = $countQueryBuilder->__toString();
        $queryBuilder->addYield();
        $query = $queryBuilder->__toString();
        $parserOptions = array_key_exists(RepositoryInterface::OPTION_KEY_PARSER_OPTIONS, $options)
            ? $options[RepositoryInterface::OPTION_KEY_PARSER_OPTIONS]
            : [];

        $query = InfluxDBHelper::addImports(
            $query,
            $options[RepositoryInterface::OPTION_KEY_IMPORT_COLLECTION] ?? [],
            $datePeriod,
            $options[RepositoryInterface::OPTION_KEY_AGGREGATE] ?? false
        );
        ($options[RepositoryInterface::OPTION_KEY_LOG_QUERY] ?? $this->config[RepositoryInterface::CONFIG_KEY_LOGGING_ENABLED])
            && $this->getLoggerByName('full')
            && $this->getLoggerByName('full')->info("\n" . $query . "\n\n\n");
        ($options[RepositoryInterface::OPTION_KEY_LOG_QUERY] ?? $this->config[RepositoryInterface::CONFIG_KEY_LOGGING_ENABLED])
            && $this->getLoggerByName('full')
            && $this->getLoggerByName('full')->info("\n" . $countQuery . "\n\n\n");

        $items = $this->connectorService->query($query, $parserOptions);
        $count = $this->connectorService->query($countQuery);

        return [
            'total' => $items->valid() ? $count[0][$countColumn ?? '_value'] : 0,
            'items' => $items
        ];
    }

    public function find($id)
    {
        return;
    }

    public function findAll()
    {
        return;
    }

    public function findOneBy(array $criteria)
    {
        return;
    }

    public function getClassName()
    {
        // TODO: Implement getClassName() method.
    }

    public function save(object $entity): void
    {
        return;
    }

    public function remove(object $entity): void
    {
        return;
    }
}
