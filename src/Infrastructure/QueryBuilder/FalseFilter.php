<?php

namespace Smtm\InfluxDB\Infrastructure\QueryBuilder;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class FalseFilter extends AbstractQueryPart
{
    public function __construct() {}

    public function __toString(): string
    {
        $comparison = $this->getComparison();

        return <<< EOT
        |> filter(fn: (r) => $comparison)
        EOT;
    }

    public function getComparison(): string
    {
        return <<< EOT
        false
        EOT;
    }
}
