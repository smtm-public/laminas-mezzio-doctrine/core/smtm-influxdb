<?php

namespace Smtm\InfluxDB\Infrastructure\QueryBuilder;

use DateInterval;
use DateTime;
use DateTimeZone;
use Smtm\Base\Infrastructure\Collection\CollectionInterface;
use Smtm\Base\Infrastructure\Collection\CollectionTrait;
use Smtm\Base\Infrastructure\Exception\RuntimeException;
use Smtm\Base\Infrastructure\Helper\BCMathHelper;
use Smtm\Base\Infrastructure\Helper\DateTimeHelper;
use Smtm\InfluxDB\Infrastructure\Enum\SeriesUnit;
use Smtm\InfluxDB\Infrastructure\Enum\TimeInterval;
use InvalidArgumentException;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class FilterCollection extends AbstractQueryPart implements CollectionInterface
{

    use CollectionTrait {
        __construct as traitConstructor;
    }

    public const CONJUNCTION_AND = 'and';
    public const CONJUNCTION_OR = 'or';

    public function __construct(
        array $values = [],
        protected string $conjunction = self::CONJUNCTION_AND
    ) {
        $this->traitConstructor($values);
    }

    public function __toString(): string
    {
        $comparison = $this->getComparison();

        return <<< EOT
        |> filter(fn: (r) => $comparison)
        EOT;
    }

    public function getComparison(): string
    {
        $filterParts = [];

        /** @var FilterCollection|Filter $filter */
        foreach ($this->container as $filter) {
            if ($filter instanceof FilterCollection) {
                $filterParts[] = '(' . $filter->getComparison() . ')';
            } else {
                $filterParts[] = $filter->getComparison();
            }
        }

        return implode(' ' . $this->conjunction . ' ', $filterParts);
    }

    public function getConjunction(): string
    {
        return $this->conjunction;
    }

    public function setConjunction(string $conjunction): static
    {
        $this->conjunction = $conjunction;

        return $this;
    }
}
