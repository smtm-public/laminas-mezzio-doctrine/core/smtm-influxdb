<?php

namespace Smtm\InfluxDB\Infrastructure\QueryBuilder;

use DateInterval;
use DateTime;
use DateTimeZone;
use Smtm\Base\Infrastructure\Exception\RuntimeException;
use Smtm\Base\Infrastructure\Helper\DateTimeHelper;
use Smtm\InfluxDB\Infrastructure\Enum\SeriesUnit;
use Smtm\InfluxDB\Infrastructure\Enum\TimeInterval;
use InvalidArgumentException;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
abstract class AbstractQueryPart implements \Stringable
{

}
