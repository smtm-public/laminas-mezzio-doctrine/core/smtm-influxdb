<?php

namespace Smtm\InfluxDB\Infrastructure\QueryBuilder;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class EmptyPart extends AbstractQueryPart
{
    public function __toString(): string
    {
        return '';
    }
}
