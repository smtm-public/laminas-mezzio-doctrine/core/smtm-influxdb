<?php

namespace Smtm\InfluxDB\Infrastructure\QueryBuilder;

use Smtm\Base\Infrastructure\Helper\BCMathHelper;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class Filter extends AbstractQueryPart
{
    public const COMPARISON_OPERATOR_REGEX = '=~';
    public const COMPARISON_OPERATOR_EQUAL = '==';
    public const COMPARISON_OPERATOR_NOT_EQUAL = '!=';
    public const COMPARISON_OPERATOR_GT = '>';
    public const COMPARISON_OPERATOR_GT_OR_EQUAL = '>=';
    public const COMPARISON_OPERATOR_LT = '<';
    public const COMPARISON_OPERATOR_LT_OR_EQUAL = '<=';

    public function __construct(
        protected string $fieldName,
        protected string|float|int|bool|null $value,
        protected string $comparisonOperator = self::COMPARISON_OPERATOR_EQUAL
    ) {}

    public function __toString(): string
    {
        $comparison = $this->getComparison();

        return <<< EOT
        |> filter(fn: (r) => $comparison)
        EOT;
    }

    public function getComparison(): string
    {
        $filteredValue = $this->getFilteredValue();

        return <<< EOT
        r["{$this->fieldName}"] {$this->comparisonOperator} $filteredValue
        EOT;
    }

    public function getFieldName(): string
    {
        return $this->fieldName;
    }

    public function setFieldName(string $fieldName): static
    {
        $this->fieldName = $fieldName;

        return $this;
    }

    public function getValue(): string|float|int|bool|null
    {
        return $this->value;
    }

    public function getFilteredValue(): string
    {
        if ($this->value === null) {
            return 'null';
        } elseif (is_string($this->value)) {
            return match ($this->comparisonOperator) {
                self::COMPARISON_OPERATOR_REGEX => $this->value,
                default => '"' . addslashes($this->value) . '"',
            };
        } elseif (is_bool($this->value)) {
            return var_export($this->value, true);
        } elseif (is_float($this->value)) {
            return BCMathHelper::castToString($this->value);
        } elseif (is_int($this->value)) {
            return (string) $this->value;
        }

        return $this->value;
    }

    public function setValue(string|float|int|bool|null $value): static
    {
        $this->value = $value;

        return $this;
    }

    public function getComparisonOperator(): string
    {
        return $this->comparisonOperator;
    }

    public function setComparisonOperator(string $comparisonOperator): static
    {
        $this->comparisonOperator = $comparisonOperator;

        return $this;
    }
}
