<?php

namespace Smtm\InfluxDB\Infrastructure\QueryBuilder\Func;

use Smtm\InfluxDB\Infrastructure\QueryBuilder\AbstractQueryPart;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class Fill extends AbstractQueryPart implements FuncInterface
{
    public function __construct(
        protected bool $usePrevious
    ) {}

    public function __toString(): string
    {
        $usePrevious = var_export($this->usePrevious, true);

        return <<< EOT
        |> fill(usePrevious: $usePrevious)
        EOT;
    }

    public function getUsePrevious(): bool
    {
        return $this->usePrevious;
    }

    public function setUsePrevious(bool $usePrevious): static
    {
        $this->usePrevious = $usePrevious;

        return $this;
    }
}
