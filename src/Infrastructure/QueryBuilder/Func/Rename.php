<?php

namespace Smtm\InfluxDB\Infrastructure\QueryBuilder\Func;

use Smtm\Base\Infrastructure\Exception\InvalidArgumentException;
use Smtm\InfluxDB\Infrastructure\QueryBuilder\AbstractQueryPart;

/**
 * @author Nikolay Nikolov <n.nikolov@smtm.bg>
 */
class Rename extends AbstractQueryPart implements FuncInterface
{
    public function __construct(
        protected ?array $columns,
        protected ?string $newColumnName,
        protected ?string $condition
    ) {}

    public function __toString(): string
    {
        switch (true) {
            case $this->columns:
                $mappedColumns = '';

                foreach ($this->columns as $oldColumn => $newColumn) {
                    $mappedColumns .= "$oldColumn: \"$newColumn\", ";
                }

                $mappedColumns = rtrim($mappedColumns, ', ');

                return '|> rename(columns: {' . $mappedColumns . '})';

            case $this->newColumnName:
                return '|> rename(fn: (column) => ${column}' . $this->newColumnName . '")';

            case $this->condition:
                return <<< EOT
                |> rename(fn: (column) => {
                    {$this->condition}
                })
                EOT;
            default:
                throw new InvalidArgumentException(sprintf('%s was provided invalid parameters', __METHOD__));
        }
    }

    public function getColumns(): ?array
    {
        return $this->columns;
    }

    public function setColumns(?array $columns): static
    {
        $this->columns = $columns;

        return $this;
    }

    public function getNewColumnName(): ?string
    {
        return $this->newColumnName;
    }

    public function setNewColumnName(?string $newColumnName): static
    {
        $this->newColumnName = $newColumnName;

        return $this;
    }

    public function getCondition(): ?string
    {
        return $this->condition;
    }

    public function setCondition(?string $condition): static
    {
        $this->condition = $condition;

        return $this;
    }
}
