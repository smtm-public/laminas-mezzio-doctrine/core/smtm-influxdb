<?php

namespace Smtm\InfluxDB\Infrastructure\QueryBuilder\Func;

use Smtm\InfluxDB\Infrastructure\QueryBuilder\AbstractQueryPart;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class Pivot extends AbstractQueryPart implements FuncInterface
{

    public function __construct(
        protected string $valueColumn,
        protected array $rowKeys = [],
        protected array $columnKeys = []
    )
    {
    }

    public function __toString(): string
    {
        $rowKeys = implode('", "', $this->rowKeys);
        $columnKeys = implode('", "', $this->columnKeys);

        return '|> pivot(rowKey:["' . $rowKeys . '"], columnKey: ["' . $columnKeys . '"], valueColumn: "' . $this->valueColumn . '")';
    }

    public function getRowKeys(): array
    {
        return $this->rowKeys;
    }

    public function setRowKeys(array $rowKeys): self
    {
        $this->rowKeys = $rowKeys;

        return $this;
    }

    public function getColumnKeys(): array
    {
        return $this->columnKeys;
    }

    public function setColumnKeys(array $columnKeys): self
    {
        $this->columnKeys = $columnKeys;

        return $this;
    }

    public function getValueColumn(): string
    {
        return $this->valueColumn;
    }

    public function setValueColumn(string $valueColumn): self
    {
        $this->valueColumn = $valueColumn;

        return $this;
    }
}
