<?php

namespace Smtm\InfluxDB\Infrastructure\QueryBuilder\Func;

use Smtm\InfluxDB\Infrastructure\QueryBuilder\AbstractQueryPart;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class KeepRegex extends AbstractQueryPart implements FuncInterface
{
    public function __construct(
        protected string $condition = '.*',
        protected bool $match = true
    ) {}

    public function __toString(): string
    {
        return '|> keep(fn (column) => column ' . ($this->match ? '=~' : '!~') . $this->condition . ')';
    }

    public function getCondition(): string
    {
        return $this->condition;
    }

    public function setCondition(string $condition): static
    {
        $this->condition = $condition;

        return $this;
    }

    public function getMatch(): bool
    {
        return $this->match;
    }

    public function setMatch(bool $match): static
    {
        $this->match = $match;

        return $this;
    }
}
