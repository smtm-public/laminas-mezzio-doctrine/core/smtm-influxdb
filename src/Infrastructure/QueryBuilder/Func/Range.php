<?php

namespace Smtm\InfluxDB\Infrastructure\QueryBuilder\Func;

use Smtm\InfluxDB\Infrastructure\QueryBuilder\AbstractQueryPart;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class Range extends AbstractQueryPart implements FuncInterface
{
    public function __construct(
        protected ?string $start = null,
        protected ?string $stop = null
    ) {}

    public function __toString(): string
    {
        return <<< EOT
        |> range(start: {$this->start}, stop: {$this->stop})
        EOT;
    }

    public function getStart(): ?string
    {
        return $this->start;
    }

    public function setStart(?string $start): static
    {
        $this->start = $start;

        return $this;
    }

    public function getStop(): ?string
    {
        return $this->stop;
    }

    public function setStop(?string $stop): static
    {
        $this->stop = $stop;

        return $this;
    }
}
