<?php

namespace Smtm\InfluxDB\Infrastructure\QueryBuilder\Func;

use Smtm\Base\Infrastructure\Helper\DateTimeHelper;
use Smtm\InfluxDB\Infrastructure\QueryBuilder\AbstractQueryPart;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class AggregateWindow extends AbstractQueryPart implements FuncInterface
{
    public const FN_SUM = 'sum';

    public function __construct(
        protected \DatePeriod $datePeriod,
        protected string $fn = self::FN_SUM,
        protected bool $createEmpty = false
    ) {}

    public function __toString(): string
    {
        $timeZoneName = $this->datePeriod->getEndDate()->getTimezone()->getName();
        $location = '';

        if ($timeZoneName !== 'UTC'
            && DateTimeHelper::compareDateIntervalToSeconds(
                $this->datePeriod->getDateInterval(),
                DateTimeHelper::DATE_INTERVAL_COMPARISON_GTE,
                24 * 60 * 60
            )
        ) {
            $location = ", location: timezone.location(name: \"$timeZoneName\")";
        }

        $interval = [];
        $dateIntervalAmountsBreakdown = DateTimeHelper::getDateIntervalAmountsBreakdown($this->datePeriod->getDateInterval());

        $interval[] = $dateIntervalAmountsBreakdown[DateTimeHelper::DATE_INTERVAL_AMOUNTS_BREAKDOWN_YEARS]
            ? $dateIntervalAmountsBreakdown[DateTimeHelper::DATE_INTERVAL_AMOUNTS_BREAKDOWN_YEARS] . 'y'
            : null;
        $interval[] = $dateIntervalAmountsBreakdown[DateTimeHelper::DATE_INTERVAL_AMOUNTS_BREAKDOWN_MONTHS]
            ? $dateIntervalAmountsBreakdown[DateTimeHelper::DATE_INTERVAL_AMOUNTS_BREAKDOWN_MONTHS] . 'mo'
            : null;
        $interval[] = $dateIntervalAmountsBreakdown[DateTimeHelper::DATE_INTERVAL_AMOUNTS_BREAKDOWN_DAYS]
            ? $dateIntervalAmountsBreakdown[DateTimeHelper::DATE_INTERVAL_AMOUNTS_BREAKDOWN_DAYS] . 'd'
            : null;
        $interval[] = $dateIntervalAmountsBreakdown[DateTimeHelper::DATE_INTERVAL_AMOUNTS_BREAKDOWN_HOURS]
            ? $dateIntervalAmountsBreakdown[DateTimeHelper::DATE_INTERVAL_AMOUNTS_BREAKDOWN_HOURS] . 'h'
            : null;
        $interval[] = $dateIntervalAmountsBreakdown[DateTimeHelper::DATE_INTERVAL_AMOUNTS_BREAKDOWN_MINUTES]
            ? $dateIntervalAmountsBreakdown[DateTimeHelper::DATE_INTERVAL_AMOUNTS_BREAKDOWN_MINUTES] . 'm'
            : null;
        $interval[] = $dateIntervalAmountsBreakdown[DateTimeHelper::DATE_INTERVAL_AMOUNTS_BREAKDOWN_SECONDS]
            ? $dateIntervalAmountsBreakdown[DateTimeHelper::DATE_INTERVAL_AMOUNTS_BREAKDOWN_SECONDS] . 's'
            : null;
        $interval[] = $dateIntervalAmountsBreakdown[DateTimeHelper::DATE_INTERVAL_AMOUNTS_BREAKDOWN_MICROSECONDS]
            ? $dateIntervalAmountsBreakdown[DateTimeHelper::DATE_INTERVAL_AMOUNTS_BREAKDOWN_MICROSECONDS] . 'us'
            : null;
        $interval = implode(array_filter($interval));
        $createEmpty = var_export($this->createEmpty, true);

        return <<< EOT
        |> aggregateWindow(every: $interval, fn: {$this->fn}, timeSrc: "_start", createEmpty: {$createEmpty}{$location})
        EOT;
    }

    public function getDatePeriod(): \DatePeriod
    {
        return $this->datePeriod;
    }

    public function setDatePeriod(\DatePeriod $datePeriod): static
    {
        $this->datePeriod = $datePeriod;

        return $this;
    }

    public function getFn(): string
    {
        return $this->fn;
    }

    public function setFn(string $fn): static
    {
        $this->fn = $fn;

        return $this;
    }

    public function getCreateEmpty(): bool
    {
        return $this->createEmpty;
    }

    public function setCreateEmpty(bool $createEmpty): static
    {
        $this->createEmpty = $createEmpty;

        return $this;
    }
}
