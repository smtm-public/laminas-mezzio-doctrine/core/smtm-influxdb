<?php

namespace Smtm\InfluxDB\Infrastructure\QueryBuilder\Func;

use Smtm\InfluxDB\Infrastructure\QueryBuilder\AbstractQueryPart;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class From extends AbstractQueryPart implements FuncInterface
{

    public function __construct(
        protected string $bucket
    ) {
    }

    public function __toString(): string
    {
        return 'from(bucket: "' . $this->bucket . '")';
    }

    public function getBucket(): string
    {
        return $this->bucket;
    }

    public function setBucket(string $bucket): self
    {
        $this->bucket = $bucket;

        return $this;
    }


}
