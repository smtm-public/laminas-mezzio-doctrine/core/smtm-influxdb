<?php

namespace Smtm\InfluxDB\Infrastructure\QueryBuilder\Func;

use Smtm\InfluxDB\Infrastructure\QueryBuilder\AbstractQueryPart;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class Limit extends AbstractQueryPart implements FuncInterface
{

    public function __construct(
        protected $limit,
        protected $offset = 0
    ) {
    }

    public function __toString(): string
    {
        return '|> limit(n: ' . $this->limit . ($this->offset ? ', offset: ' . $this->offset : '') . ')';
    }

    /**
     * @return mixed
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @param mixed $limit
     */
    public function setLimit($limit): self
    {
        $this->limit = $limit;

        return $this;
    }

    public function getOffset(): int
    {
        return $this->offset;
    }

    public function setOffset(int $offset): self
    {
        $this->offset = $offset;

        return $this;
    }

}
