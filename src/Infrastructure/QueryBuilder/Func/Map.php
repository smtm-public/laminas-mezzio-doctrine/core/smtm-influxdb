<?php

namespace Smtm\InfluxDB\Infrastructure\QueryBuilder\Func;

use Smtm\InfluxDB\Infrastructure\QueryBuilder\AbstractQueryPart;

/**
 * @author Nikolay Nikolov <n.nikolov@smtm.bg>
 */
class Map extends AbstractQueryPart implements FuncInterface
{
    public function __construct(
        protected bool $with,
        protected string $outputRecord,
        protected ?string $scopedVariables
    ) {}

    public function __toString(): string
    {
        $with = $this->with ? 'r with' : '';

        if ($this->scopedVariables) {
            $function = <<< EOT
            |> map(
                fn: (r) => {
                  $this->scopedVariables
            
                  return {{$with} $this->outputRecord}
                }
              )
            EOT;
        } else {
            $function = <<< EOT
            |> map(
                fn: (r) => ({{$with} $this->outputRecord})
              )
            EOT;
        }

        return $function;
    }

    public function getWith(): bool
    {
        return $this->with;
    }

    public function setWith(bool $with): static
    {
        $this->with = $with;

        return $this;
    }

    public function getOutputRecord(): string
    {
        return $this->outputRecord;
    }

    public function setOutputRecord(string $outputRecord): static
    {
        $this->outputRecord = $outputRecord;

        return $this;
    }

    public function getScopedVariables(): ?string
    {
        return $this->scopedVariables;
    }

    public function setScopedVariables(?string $scopedVariables): static
    {
        $this->scopedVariables = $scopedVariables;

        return $this;
    }
}
