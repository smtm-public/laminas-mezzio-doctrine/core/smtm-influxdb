<?php

namespace Smtm\InfluxDB\Infrastructure\QueryBuilder\Func;

use Smtm\InfluxDB\Infrastructure\QueryBuilder\AbstractQueryPart;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class Count extends AbstractQueryPart implements FuncInterface
{

    public function __construct(
        protected ?string $column = null,
    ) {
    }

    public function __toString(): string
    {
        if ($this->column) {
            return "|> count(column: \"$this->column\")";
        }
        return '|> count()';
    }
}
