<?php

namespace Smtm\InfluxDB\Infrastructure\QueryBuilder\Func;

use Smtm\InfluxDB\Infrastructure\QueryBuilder\AbstractQueryPart;

/**
 * @author Nikolay Nikolov <n.nikolov@smtm.bg>
 */
class ToString extends AbstractQueryPart implements FuncInterface
{
    public function __toString(): string
    {
        return '|> toString()';
    }
}
