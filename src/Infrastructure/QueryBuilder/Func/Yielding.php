<?php

namespace Smtm\InfluxDB\Infrastructure\QueryBuilder\Func;

use Smtm\InfluxDB\Infrastructure\QueryBuilder\AbstractQueryPart;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class Yielding extends AbstractQueryPart implements FuncInterface
{

    public function __construct()
    {
    }

    public function __toString(): string
    {
        return '|> yield()';
    }
}
