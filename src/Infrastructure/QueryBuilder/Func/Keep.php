<?php

namespace Smtm\InfluxDB\Infrastructure\QueryBuilder\Func;

use Smtm\InfluxDB\Infrastructure\QueryBuilder\AbstractQueryPart;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class Keep extends AbstractQueryPart implements FuncInterface
{
    public function __construct(
        protected array $columns = []
    ) {}

    public function __toString(): string
    {
        $columns = implode('", "', $this->columns);

        return '|> keep(columns:["' . $columns . '"])';
    }

    public function getColumns(): array
    {
        return $this->columns;
    }

    public function setColumns(array $columns): static
    {
        $this->columns = $columns;

        return $this;
    }
}
