<?php

namespace Smtm\InfluxDB\Infrastructure\QueryBuilder\Func;

use Smtm\InfluxDB\Infrastructure\QueryBuilder\AbstractQueryPart;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class Sort extends AbstractQueryPart implements FuncInterface
{
    public function __construct(
        protected array $columns,
        protected bool $desc = false
    ) {}

    public function __toString(): string
    {
        $desc = var_export($this->desc, true);
        $columns = implode('", "', $this->columns);

        return '|> sort(columns: ["' . $columns . '"], desc: ' . $desc . ')';
    }

    public function getColumns(): array
    {
        return $this->columns;
    }

    public function setColumns(array $columns): static
    {
        $this->columns = $columns;

        return $this;
    }

    public function isDesc(): bool
    {
        return $this->desc;
    }

    public function setDesc(bool $desc): static
    {
        $this->desc = $desc;

        return $this;
    }
}
