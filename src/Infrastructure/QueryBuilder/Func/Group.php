<?php

namespace Smtm\InfluxDB\Infrastructure\QueryBuilder\Func;

use Smtm\InfluxDB\Infrastructure\QueryBuilder\AbstractQueryPart;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class Group extends AbstractQueryPart implements FuncInterface
{

    public function __construct(
        protected ?array $columns = []
    )
    {
    }

    public function __toString(): string
    {
        $columns = $this->getColumns();

        if ($columns) {
            $columns = implode('", "', $columns);

            return "|> group(columns: [\"$columns\"])";
        }

        return '|> group()';
    }

    public function getColumns(): ?array
    {
        return $this->columns;
    }
}
