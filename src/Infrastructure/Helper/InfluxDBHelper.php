<?php

namespace Smtm\InfluxDB\Infrastructure\Helper;

use DateInterval;
use DateTime;
use DateTimeZone;
use Smtm\Base\Infrastructure\Exception\RuntimeException;
use Smtm\Base\Infrastructure\Helper\DateTimeHelper;
use Smtm\InfluxDB\Infrastructure\Enum\SeriesUnit;
use Smtm\InfluxDB\Infrastructure\Enum\TimeInterval;
use Smtm\InfluxDB\Infrastructure\QueryBuilder;
use Smtm\InfluxDB\Infrastructure\QueryBuilder\AbstractQueryPart;
use Smtm\InfluxDB\Infrastructure\QueryBuilder\EmptyPart;
use Smtm\InfluxDB\Infrastructure\QueryBuilder\FalseFilter;
use Smtm\InfluxDB\Infrastructure\QueryBuilder\Filter;
use Smtm\InfluxDB\Infrastructure\QueryBuilder\FilterCollection;
use Smtm\InfluxDB\Infrastructure\QueryBuilder\Func\Range;
use Smtm\InfluxDB\Infrastructure\QueryBuilder\Func\FuncInterface;
use InvalidArgumentException;

/**
 * @author Nikolay Nikolov <n.nikolov@smtm.bg>
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class InfluxDBHelper
{
    private const INFLUX_TIME_FORMAT = 'Y-m-d\TH:i:s\Z';

    public static function resolveCriteria(
        QueryBuilder $queryBuilder,
        array $criteria,
        array $fieldNameMap = [],
        string $currentConjunction = 'and'
    ): ?AbstractQueryPart {
        $partCount = count($criteria);
        $resolvedParts = [];

        if (isset($criteria[0]) && in_array($criteria[0], ['and', 'or'], true)) {
            $currentConjunction = $criteria[0];

            /**
             * [
             *   'and',
             *   ['property0' => 'value0'],
             *   ['=', 'property1', 'value1'],
             *   [
             *     ['property2' => 'value2'],
             *     ['=', 'property3', 'value3'],
             *     ...
             *   ],
             *   ...
             * ]
             */
            return static::resolveCriteria(
                $queryBuilder,
                array_slice($criteria, 1),
                $fieldNameMap,
                $currentConjunction
            );
        } elseif (
            $partCount === 3
            && array_key_exists(0, $criteria)
            && array_key_exists(1, $criteria)
            && array_key_exists(2, $criteria)
            && is_string($criteria[0])
        ) {
            if (str_contains($criteria[1], '.')) {
                /**
                 * ['=', 'relation.property', 'value']
                 */
                return static::buildComparison(
                    $criteria[0],
                    static::getFieldIdentifier(
                        $fieldNameMap,
                        $criteria[1]
                    ),
                    $criteria[2]
                );
            } else {
                /**
                 * ['=', 'property', 'value']
                 */
                return static::buildComparison(
                    $criteria[0],
                    static::getFieldIdentifier(
                        $fieldNameMap,
                        $criteria[1]
                    ),
                    $criteria[2]
                );
            }
        } else {
            /**
             * ['=', 'property0', 'value0'],
             * ['property1' => 'value1']
             * [
             *   ['property2' => 'value2'],
             *   ['=', 'property3', 'value3']
             *   ...
             * ]
             * ...
             */
            foreach ($criteria as $childIndex => $childCriterion) {
                $resolvedPart = null;

                if (is_array($childCriterion)) {
                    /**
                     * ['=', 'property', 'value']
                     *
                     * - or -
                     *
                     * [
                     *   'and',
                     *   ['=', 'property0', 'value0'],
                     *   ['property1' => 'value1']
                     *   ...
                     * ]
                     */
                    $resolvedPart =
                        static::resolveCriteria(
                            $queryBuilder,
                            $childCriterion,
                            $fieldNameMap,
                            $currentConjunction
                        );
                } elseif (!is_numeric($childIndex)) {
                    /**
                     * ['property' => 'value']
                     */
                    $resolvedPart =
                        static::resolveCriteria(
                            $queryBuilder,
                            ['=', $childIndex, $childCriterion],
                            $fieldNameMap,
                            $currentConjunction
                        );
                }

                if ($resolvedPart !== null && !$resolvedPart instanceof EmptyPart) {
                    $resolvedParts[] = $resolvedPart;
                }
            }

            $filters = [];

            foreach ($resolvedParts as $resolvedPart) {
                if ($resolvedPart instanceof FuncInterface) {
                    $queryBuilder->push($resolvedPart);
                } else {
                    $filters[] = $resolvedPart;
                }
            }

            return !empty($filters)
                ? (count($filters) > 1 ? new FilterCollection($filters, $currentConjunction) : $filters[array_key_first($filters)])
                : null;
        }
    }

    public static function getDatePeriodFromCriteria(
        array $criteria,
        string $fieldName
    ): ?\DatePeriod {
        $partCount = count($criteria);

        if (isset($criteria[0]) && in_array($criteria[0], ['and', 'or'], true)) {
            /**
             * [
             *   'and',
             *   ['property0' => 'value0'],
             *   ['=', 'property1', 'value1'],
             *   [
             *     ['property2' => 'value2'],
             *     ['=', 'property3', 'value3'],
             *     ...
             *   ],
             *   ...
             * ]
             */
            return static::getDatePeriodFromCriteria(
                array_slice($criteria, 1),
                $fieldName
            );
        } elseif (
            /**
             * ['=', 'relation', 'value']
             */
            $partCount === 3
            && array_key_exists(0, $criteria)
            && array_key_exists(1, $criteria)
            && array_key_exists(2, $criteria)
            && is_string($criteria[0])
        ) {
            if ($criteria[1] === $fieldName) {
                /**
                 * ['=', 'relation.property', 'value']
                 */
                return $criteria[2];
            }
        } else {
            /**
             * ['=', 'property0', 'value0'],
             * ['property1' => 'value1']
             * [
             *   ['property2' => 'value2'],
             *   ['=', 'property3', 'value3']
             *   ...
             * ]
             * ...
             */
            foreach ($criteria as $childIndex => $childCriterion) {
                if (is_array($childCriterion)) {
                    /**
                     * ['=', 'property', 'value']
                     *
                     * - or -
                     *
                     * [
                     *   'and',
                     *   ['=', 'property0', 'value0'],
                     *   ['property1' => 'value1']
                     *   ...
                     * ]
                     */
                    return static::getDatePeriodFromCriteria($childCriterion, $fieldName);
                } elseif (!is_numeric($childIndex)) {
                    /**
                     * ['property' => 'value']
                     */
                    if ($childIndex === $fieldName) {
                        return $childCriterion;
                    }
                }
            }
        }

        return null;
    }

    public static function buildComparison(
        string $comparator,
        string $field,
        object|array|string|int|bool|null $value
    ): AbstractQueryPart {
        if ($value instanceof \DatePeriod) {
            //return static::getRangeForDatePeriod($value) . "\n" . static::getAggregateWindowFnFromDatePeriod($value);
            return static::getQueryPartRangeForDatePeriod($value);
        }

        $comparison = new EmptyPart();

        if ($comparator === 'is null'
            || (is_null($value) && $comparator === '=')
        ) {
            $comparison = new Filter($field, null);
        } elseif ($comparator === 'is not null'
            || (is_null($value) && $comparator === '!=')
        ) {
            $comparison = new Filter($field, null, Filter::COMPARISON_OPERATOR_NOT_EQUAL);
        } elseif ($comparator === '=~') {
            $comparison = new Filter($field, $value, Filter::COMPARISON_OPERATOR_REGEX);
        } elseif ($comparator === '=') {
            $comparison = new Filter($field, $value);
        } elseif ($comparator === '!=') {
            $comparison = new Filter($field, $value, Filter::COMPARISON_OPERATOR_NOT_EQUAL);
        } elseif ($comparator === 'like') {
            $comparison = new Filter($field, $value);
        } elseif ($comparator === 'like%') {
            $comparison = new Filter($field, $value);
        } elseif ($comparator === '%like') {
            $comparison = new Filter($field, $value);
        } elseif ($comparator === 'not like') {
            $comparison = new Filter($field, $value, Filter::COMPARISON_OPERATOR_NOT_EQUAL);
        } elseif ($comparator === 'not like%') {
            $comparison = new Filter($field, $value, Filter::COMPARISON_OPERATOR_NOT_EQUAL);
        } elseif ($comparator === '%not like') {
            $comparison = new Filter($field, $value, Filter::COMPARISON_OPERATOR_NOT_EQUAL);
        } elseif ($comparator === 'in') {
            if (is_object($value)) {
                $comparison = new Filter($field, (string) $value);
            } elseif (is_iterable($value)) {
                if (empty($value)) {
                    $comparison = new FalseFilter();
                } else {
                    $filterCollection = [];

                    foreach ($value as $valueItem) {
                        $filterCollection[] = new Filter($field, $valueItem);
                    }

                    $comparison = new FilterCollection($filterCollection, FilterCollection::CONJUNCTION_OR);
                }
            }
        } elseif ($comparator === 'not in') {
            if (is_iterable($value)) {
                $filterCollection = [];

                foreach ($value as $valueItem) {
                    $filterCollection[] = new Filter($field, $valueItem, Filter::COMPARISON_OPERATOR_NOT_EQUAL);
                }

                $comparison = new FilterCollection($filterCollection);
            }
        } elseif ($comparator === '>') {
            $comparison = new Filter($field, $value, Filter::COMPARISON_OPERATOR_GT);
        } elseif ($comparator === '>=') {
            $comparison = new Filter($field, $value, Filter::COMPARISON_OPERATOR_GT_OR_EQUAL);
        } elseif ($comparator === '<') {
            $comparison = new Filter($field, $value, Filter::COMPARISON_OPERATOR_LT);
        } elseif ($comparator === '<=') {
            $comparison = new Filter($field, $value, Filter::COMPARISON_OPERATOR_LT_OR_EQUAL);
        } else {
            throw new RuntimeException('Unsupported operator');
        }

        return $comparison;
    }

    public static function getFieldIdentifier(
        array $fieldNameMap,
        string $fieldName
    ): string {
        if (array_key_exists($fieldName, $fieldNameMap)) {
            return $fieldNameMap[$fieldName];
        }

        return $fieldName;
    }

    public static function getConditionalFilter(string $key, array $values, string $logicalOperator = 'or'): string
    {
        $conditions = !empty($values)
            ? implode(" $logicalOperator ", array_map(fn(string $value) => "r[\"$key\"] == \"$value\"", $values))
            : "r[\"$key\"] == \"0\"";

        return "|> filter(fn: (r) => $conditions)";
    }

    public static function getParserColumnCallbacks(
        array $columns,
        ?string $convertToTimezone = null,
        ?string $unit = null
    ): array {
        $columnCallbacks = [];
        $columnsConvertedToDateTime = ['date', 'time'];

        if ((in_array('date', $columns) || in_array('time', $columns)) && empty($convertToTimezone)) {
            throw new InvalidArgumentException(sprintf(
                '%s expects "convertToTimezone" parameter should be passed and cannot be empty for "%s" column',
                __METHOD__,
                implode(' or ', $columnsConvertedToDateTime)
            ));
        }

        foreach ($columns as $column) {
            if (in_array($column, $columnsConvertedToDateTime)) {
                $columnCallbacks[$column] = [
                    'method' => 'convertDateTime',
                    'params' => [
                        'timezoneTo' => $convertToTimezone,
                        'formatTo' => DateTime::ATOM
                    ]
                ];
            } else {
                $columnCallbacks[$column] = [
                    'method' => 'convertValueToUnit',
                    'params' => [
                        'unit' => $unit ?? SeriesUnit::MWH->value
                    ]
                ];
            }
        }

        return $columnCallbacks;
    }

    public static function convertDateTimeParamsToUtcAndGetInterval(
        string $start,
        string $stop,
        string $localTimezone = 'Europe/Sofia',
        bool $getPreviousPeriodRange = false
    ): array {
        $localTimezone = new DateTimeZone($localTimezone);
        $utcTimezone = new DateTimeZone('UTC');
        $startDateTime = DateTime::createFromFormat('Y-m-d', $start, $localTimezone)
            ->setTime(0, 0)
            ->setTimezone($utcTimezone);
        $stopDateTime = DateTime::createFromFormat('Y-m-d', $stop, $localTimezone)
            ->add(new DateInterval('P1D'))
            ->setTime(0, 0)
            ->setTimezone($utcTimezone);
        $diffInDays = $startDateTime->diff($stopDateTime)->days;
        $data = [
            'startTime' => $startDateTime->format(self::INFLUX_TIME_FORMAT),
            'stopTime' => $stopDateTime->format(self::INFLUX_TIME_FORMAT),
            'interval' => $diffInDays > 1 ? TimeInterval::ONE_DAY->value : TimeInterval::ONE_HOUR->value
        ];

        if ($getPreviousPeriodRange) {
            if ($diffInDays > 7) {
                $dateTimeObj = $startDateTime->sub(new DateInterval('P1M'));
                $previousPeriodStartTime = (clone $dateTimeObj)->modify('first day of this month');
                $previousPeriodStopTime = $dateTimeObj
                    ->modify('last day of this month')
                    ->add(new DateInterval('P1D'))
                    ->format(self::INFLUX_TIME_FORMAT);
            } else {
                $previousPeriodStartTime = $startDateTime->sub(new DateInterval("P{$diffInDays}D"));
                $previousPeriodStopTime = $data['startTime'];
            }

            $data = array_merge(
                $data,
                [
                    'previousPeriodStartTime' => $previousPeriodStartTime->format(self::INFLUX_TIME_FORMAT),
                    'previousPeriodStopTime' => $previousPeriodStopTime
                ]
            );
        }

        return $data;
    }

    public static function getAggregateWindowFn(
        string $interval,
        string $selectorFn = 'sum',
        ?string $location = null,
        bool $createEmpty = false
    ): string {
        $createEmpty = var_export($createEmpty, true);
        $aggregateWindow = "|> aggregateWindow(every: $interval, fn: $selectorFn, timeSrc: \"_start\", createEmpty: $createEmpty)";

        if ($location && $interval === TimeInterval::ONE_DAY->value) {
            $aggregateWindow = rtrim($aggregateWindow, ')') . ", location: timezone.location(name: \"$location\"))";
        }

        return $aggregateWindow;
    }

    public static function getAggregateWindowFnFromDatePeriod(
        \DatePeriod $datePeriod,
        string $selectorFn = 'sum',
        bool $createEmpty = false,
        bool $usePrevious = false
    ): string {
        $timeZoneName = $datePeriod->getEndDate()->getTimezone()->getName();

        $interval = [];
        $dateIntervalAmountsBreakdown = DateTimeHelper::getDateIntervalAmountsBreakdown($datePeriod->getDateInterval());

        $interval[] = $dateIntervalAmountsBreakdown[DateTimeHelper::DATE_INTERVAL_AMOUNTS_BREAKDOWN_YEARS]
            ? $dateIntervalAmountsBreakdown[DateTimeHelper::DATE_INTERVAL_AMOUNTS_BREAKDOWN_YEARS] . 'y'
            : null;
        $interval[] = $dateIntervalAmountsBreakdown[DateTimeHelper::DATE_INTERVAL_AMOUNTS_BREAKDOWN_MONTHS]
            ? $dateIntervalAmountsBreakdown[DateTimeHelper::DATE_INTERVAL_AMOUNTS_BREAKDOWN_MONTHS] . 'mo'
            : null;
        $interval[] = $dateIntervalAmountsBreakdown[DateTimeHelper::DATE_INTERVAL_AMOUNTS_BREAKDOWN_DAYS]
            ? $dateIntervalAmountsBreakdown[DateTimeHelper::DATE_INTERVAL_AMOUNTS_BREAKDOWN_DAYS] . 'd'
            : null;
        $interval[] = $dateIntervalAmountsBreakdown[DateTimeHelper::DATE_INTERVAL_AMOUNTS_BREAKDOWN_HOURS]
            ? $dateIntervalAmountsBreakdown[DateTimeHelper::DATE_INTERVAL_AMOUNTS_BREAKDOWN_HOURS] . 'h'
            : null;
        $interval[] = $dateIntervalAmountsBreakdown[DateTimeHelper::DATE_INTERVAL_AMOUNTS_BREAKDOWN_MINUTES]
            ? $dateIntervalAmountsBreakdown[DateTimeHelper::DATE_INTERVAL_AMOUNTS_BREAKDOWN_MINUTES] . 'm'
            : null;
        $interval[] = $dateIntervalAmountsBreakdown[DateTimeHelper::DATE_INTERVAL_AMOUNTS_BREAKDOWN_SECONDS]
            ? $dateIntervalAmountsBreakdown[DateTimeHelper::DATE_INTERVAL_AMOUNTS_BREAKDOWN_SECONDS] . 's'
            : null;
        $interval[] = $dateIntervalAmountsBreakdown[DateTimeHelper::DATE_INTERVAL_AMOUNTS_BREAKDOWN_MICROSECONDS]
            ? $dateIntervalAmountsBreakdown[DateTimeHelper::DATE_INTERVAL_AMOUNTS_BREAKDOWN_MICROSECONDS] . 'us'
            : null;
        $interval = implode(array_filter($interval));

        $createEmpty = var_export($createEmpty, true);
        $aggregateWindow = <<< EOT
          |> group()
          |> aggregateWindow(every: $interval, fn: $selectorFn, timeSrc: "_start", createEmpty: $createEmpty)
        EOT;

        if ($timeZoneName !== 'UTC'
            && DateTimeHelper::compareDateIntervalToSeconds(
                $datePeriod->getDateInterval(),
                DateTimeHelper::DATE_INTERVAL_COMPARISON_GTE,
                24 * 60 * 60
            )
        ) {
            $aggregateWindow = rtrim($aggregateWindow, ')') . ", location: timezone.location(name: \"$timeZoneName\"))";
        }

        if ($usePrevious) {
            $aggregateWindow .= "\n" . '  |> fill(usePrevious: true)';
        }

        return $aggregateWindow . "\n";
    }

    public static function getRangeForDatePeriod(\DatePeriod $datePeriod): string
    {
        $from = $datePeriod->getStartDate()->format('c');
        $to = $datePeriod->getEndDate()->format('c');

        return <<< EOT
          |> range(start: $from, stop: $to)
        EOT;
    }

    public static function getQueryPartRangeForDatePeriod(\DatePeriod $datePeriod): Range
    {
        $start = $datePeriod->getStartDate()->format('c');
        $stop = $datePeriod->getEndDate()->format('c');

        return new Range($start, $stop);
    }

    public static function addImports(
        string $query,
        array $importCollection = [],
        ?\DatePeriod $datePeriod = null,
        string | bool | null $aggregation = false
    ): string {
        $imports = null;

        if ($datePeriod !== null && $aggregation) {
            $timeZoneName = $datePeriod->getEndDate()->getTimezone()->getName();

            if ($timeZoneName !== 'UTC'
                && DateTimeHelper::compareDateIntervalToSeconds(
                    $datePeriod->getDateInterval(),
                    DateTimeHelper::DATE_INTERVAL_COMPARISON_GTE,
                    24 * 60 * 60
                )
            ) {
                $importCollection[] = 'timezone';
            }
        }

        foreach ($importCollection as $import) {
            $imports .= "import \"$import\"\n";
        }

        return $imports ? $imports . "\n" . $query : $query;
    }
}
