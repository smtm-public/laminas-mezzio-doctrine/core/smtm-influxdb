<?php

declare(strict_types=1);

namespace Smtm\InfluxDB\Infrastructure;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait RepositoryAwareTrait
{
    protected AbstractRepository $repository;

    public function getRepository(): AbstractRepository
    {
        return $this->repository;
    }

    public function setRepository(AbstractRepository $repository): static
    {
        $this->repository = $repository;

        return $this;
    }
}
