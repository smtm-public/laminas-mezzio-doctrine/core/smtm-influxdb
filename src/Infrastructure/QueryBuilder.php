<?php

namespace Smtm\InfluxDB\Infrastructure;

use Smtm\Base\Infrastructure\Collection\CollectionTrait;
use Smtm\InfluxDB\Infrastructure\QueryBuilder\AbstractQueryPart;
use Smtm\InfluxDB\Infrastructure\QueryBuilder\Func\AggregateWindow;
use Smtm\InfluxDB\Infrastructure\QueryBuilder\Func\Count;
use Smtm\InfluxDB\Infrastructure\QueryBuilder\Func\Fill;
use Smtm\InfluxDB\Infrastructure\QueryBuilder\Func\From;
use Smtm\InfluxDB\Infrastructure\QueryBuilder\Func\Group;
use Smtm\InfluxDB\Infrastructure\QueryBuilder\Func\Keep;
use Smtm\InfluxDB\Infrastructure\QueryBuilder\Func\KeepRegex;
use Smtm\InfluxDB\Infrastructure\QueryBuilder\Func\Limit;
use Smtm\InfluxDB\Infrastructure\QueryBuilder\Func\Map;
use Smtm\InfluxDB\Infrastructure\QueryBuilder\Func\Pivot;
use Smtm\InfluxDB\Infrastructure\QueryBuilder\Func\Rename;
use Smtm\InfluxDB\Infrastructure\QueryBuilder\Func\Sort;
use Smtm\InfluxDB\Infrastructure\QueryBuilder\Func\ToString;
use Smtm\InfluxDB\Infrastructure\QueryBuilder\Func\Yielding;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class QueryBuilder implements \Stringable
{

    use CollectionTrait;

    public const DEFAULT_INDENTATION = '  ';

    protected string $indentation = self::DEFAULT_INDENTATION;

    public function __toString(): string
    {
        return implode(
            "\n",
            array_map(
                fn (AbstractQueryPart $part) => $part instanceof From ? (string)$part : $this->indentation . $part,
                $this->container
            )
        );
    }

    public function addAggregation(
        \DatePeriod $datePeriod,
        string $selectorFn = 'sum',
        bool $createEmpty = false,
        bool $usePrevious = false
    ): static {
        $this->push(new AggregateWindow($datePeriod, $selectorFn, $createEmpty));

        if ($usePrevious) {
            $this->push(new Fill(true));
        }

        return $this;
    }

    public function addLimit(
        int $limit,
        int $offset = 0
    ): static {
        $this->push(new Limit($limit, $offset));

        return $this;
    }

    public function addSort(
        array $columns,
        bool $desc = false
    ): static {
        $this->push(new Sort($columns, $desc));

        return $this;
    }

    public function addCount(?string $column = null): static
    {
        $this->push(new Count($column));

        return $this;
    }

    public function addYield(): static
    {
        $this->push(new Yielding());

        return $this;
    }

    public function addGroup(?array $columns = []): static
    {
        $this->push(new Group($columns));

        return $this;
    }

    public function addFrom(
        string $bucket,
    ): static {
        $this->push(new From($bucket));

        return $this;
    }

    public function getIndentation(): string
    {
        return $this->indentation;
    }

    public function setIndentation(string $indentation): static
    {
        $this->indentation = $indentation;

        return $this;
    }

    public function addPivot(
        string $valueColumn,
        array $rowKeys,
        array $valueKeys
    ): static {
        $this->push(new Pivot($valueColumn, $rowKeys, $valueKeys));

        return $this;
    }

    public function addKeep(
        array $columns
    ): static {
        $this->push(new Keep($columns));

        return $this;
    }

    public function addKeepRegex(
        string $condition,
        bool $match = true
    ): static {
        $this->push(new KeepRegex($condition, $match));

        return $this;
    }

    public function addMap(bool $with, string $outputRecord, ?string $scopedVariables = null): static
    {
        $this->push(new Map($with, $outputRecord, $scopedVariables));

        return $this;
    }

    public function addRename(
        ?array $columns = null,
        ?string $newColumnName = null,
        ?string $condition = null
    ): static {
        $this->push(new Rename($columns, $newColumnName, $condition));

        return $this;
    }

    public function addToString(): static
    {
        $this->push(new ToString());

        return $this;
    }
}
