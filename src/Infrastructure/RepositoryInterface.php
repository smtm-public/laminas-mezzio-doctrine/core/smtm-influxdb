<?php

declare(strict_types=1);

namespace Smtm\InfluxDB\Infrastructure;

use Smtm\Base\Infrastructure\Repository\RepositoryInterface as BaseRepositoryInterface;
use JetBrains\PhpStorm\ArrayShape;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface RepositoryInterface extends BaseRepositoryInterface
{
    public const CONFIG_KEY_LOGGING_ENABLED = 'loggingEnabled';

    public const OPTION_KEY_LOG_QUERY = 'logQuery';
    public const OPTION_KEY_AGGREGATE = 'aggregate';
    public const OPTION_KEY_CREATE_EMPTY = 'createEmpty';
    public const OPTION_KEY_USE_PREVIOUS = 'usePrevious';
    public const OPTION_KEY_PIVOT = 'pivot';
    public const OPTION_KEY_SKIP_SORTING = 'skipSorting';
    public const OPTION_KEY_PARSER_OPTIONS = 'parserOptions';
    public const OPTION_KEY_CUSTOM_GROUP = 'customGroup';
    public const OPTION_KEY_KEEP = 'keep';
    public const OPTION_KEY_COUNT_COLUMN = 'countColumn';
    public const OPTION_KEY_IMPORT_COLLECTION = 'importCollection';
    public const OPTION_KEY_MAP = 'map';
    public const OPTION_KEY_RENAME = 'rename';
    public const OPTION_KEY_TO_STRING = 'toString';

    /**
     * THE DATE PERIOD CRITERION MUST BE THE FIRST ELEMENT IN THE CRITERIA ARRAY
     */
    public function findBy(
        array $criteria,
        ?array $orderBy = null,
        ?int $limit = null,
        ?int $offset = null,
        #[ArrayShape([
            BaseRepositoryInterface::OPTION_KEY_CACHE_STORAGE => 'string',
            BaseRepositoryInterface::OPTION_KEY_HYDRATION_MODE => 'int | null',
            BaseRepositoryInterface::OPTION_KEY_RESULT_COLLECTION_INDEX_BY => 'string | null',
            self::OPTION_KEY_AGGREGATE => 'bool',
            self::OPTION_KEY_CREATE_EMPTY => 'bool',
            self::OPTION_KEY_USE_PREVIOUS => 'bool',
            self::OPTION_KEY_PIVOT => 'array',
            self::OPTION_KEY_SKIP_SORTING => 'bool',
            self::OPTION_KEY_PARSER_OPTIONS => ParserOptions::class . '|array',
            self::OPTION_KEY_CUSTOM_GROUP => 'array',
            self::OPTION_KEY_KEEP => 'array',
            self::OPTION_KEY_LOG_QUERY => 'bool | null',
            self::OPTION_KEY_IMPORT_COLLECTION => 'array',
            self::OPTION_KEY_MAP => 'array',
            self::OPTION_KEY_RENAME => 'array',
            self::OPTION_KEY_TO_STRING => 'bool | null',
        ])] array $options = []
    ): array|\Generator;

    public function buildQuery(
        array $criteria,
        ?array $orderBy = null,
        ?int $limit = null,
        ?int $offset = null,
        #[ArrayShape([
            BaseRepositoryInterface::OPTION_KEY_CACHE_STORAGE => 'string',
            BaseRepositoryInterface::OPTION_KEY_HYDRATION_MODE => 'int | null',
            BaseRepositoryInterface::OPTION_KEY_RESULT_COLLECTION_INDEX_BY => 'string | null',
            self::OPTION_KEY_AGGREGATE => 'bool',
            self::OPTION_KEY_CREATE_EMPTY => 'bool',
            self::OPTION_KEY_USE_PREVIOUS => 'bool',
            self::OPTION_KEY_PIVOT => 'array',
            self::OPTION_KEY_SKIP_SORTING => 'bool',
            self::OPTION_KEY_PARSER_OPTIONS => ParserOptions::class . '|array',
            self::OPTION_KEY_CUSTOM_GROUP => 'array',
            self::OPTION_KEY_KEEP => 'array',
            self::OPTION_KEY_LOG_QUERY => 'bool | null',
            self::OPTION_KEY_IMPORT_COLLECTION => 'array',
            self::OPTION_KEY_MAP => 'array',
            self::OPTION_KEY_RENAME => 'array',
            self::OPTION_KEY_TO_STRING => 'bool | null',
        ])] array $options = []
    ): string;
}
