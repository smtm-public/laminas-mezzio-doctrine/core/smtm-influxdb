<?php

declare(strict_types=1);

namespace Smtm\InfluxDB\Infrastructure\Factory;

use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Smtm\InfluxDB\Infrastructure\AbstractRepository;
use Laminas\ServiceManager\Factory\AbstractFactoryInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class RepositoryAbstractFactory implements AbstractFactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new $requestedName(
            $container->get(InfrastructureServicePluginManager::class),
            $container->get(InfrastructureServicePluginManager::class)->get($options['connectorService']),
            $options['bucket'],
            $options['config']
        );
    }

    public function canCreate(ContainerInterface $container, $requestedName)
    {
        return is_subclass_of($requestedName, AbstractRepository::class);
    }
}
