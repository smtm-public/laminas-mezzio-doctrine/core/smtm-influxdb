<?php

declare(strict_types=1);

namespace Smtm\InfluxDB\Infrastructure;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ParserOptions
{
    public function __construct(
        protected ?array $includedColumns = null,
        protected ?array $columnCallbacks = null,
        protected ?array $castValues = null,
        protected ?array $indexOptions = null,
        protected ?bool $returnAsIterable = null
    ) {

    }

    public function toArray(): array
    {
        return array_filter(
            [
                'includedColumns' => $this->includedColumns,
                'columnCallbacks' => $this->columnCallbacks,
                'castValues' => $this->castValues,
                'indexOptions' => $this->indexOptions,
                'returnAsIterable' => $this->returnAsIterable,
            ],
            fn (?array $element) => $element !== null
        );
    }

    public function fromArray(array $data): static
    {
        if (array_key_exists('includedColumns', $data)) {
            $this->includedColumns = $data['includedColumns'];
        }

        if (array_key_exists('columnCallbacks', $data)) {
            $this->columnCallbacks = $data['columnCallbacks'];
        }

        if (array_key_exists('castValues', $data)) {
            $this->castValues = $data['castValues'];
        }

        if (array_key_exists('indexOptions', $data)) {
            $this->indexOptions = $data['indexOptions'];
        }

        if (array_key_exists('returnAsIterable', $data)) {
            $this->returnAsIterable = $data['returnAsIterable'];
        }

        return $this;
    }

    public function getIncludedColumns(): ?array
    {
        return $this->includedColumns;
    }

    public function setIncludedColumns(?array $includedColumns): static
    {
        $this->includedColumns = $includedColumns;

        return $this;
    }

    public function getColumnCallbacks(): ?array
    {
        return $this->columnCallbacks;
    }

    public function setColumnCallbacks(?array $columnCallbacks): static
    {
        $this->columnCallbacks = $columnCallbacks;

        return $this;
    }

    public function getCastValues(): ?array
    {
        return $this->castValues;
    }

    public function setCastValues(?array $castValues): static
    {
        $this->castValues = $castValues;

        return $this;
    }

    public function getIndexOptions(): ?array
    {
        return $this->indexOptions;
    }

    public function setIndexOptions(?array $indexOptions): static
    {
        $this->indexOptions = $indexOptions;

        return $this;
    }

    public function getReturnAsIterable(): ?bool
    {
        return $this->returnAsIterable;
    }

    public function setReturnAsIterable(?bool $returnAsIterable): static
    {
        $this->returnAsIterable = $returnAsIterable;

        return $this;
    }
}
