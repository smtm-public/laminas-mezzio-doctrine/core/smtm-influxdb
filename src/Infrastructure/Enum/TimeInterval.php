<?php

namespace Smtm\InfluxDB\Infrastructure\Enum;

/**
 * @author Nikolay Nikolov <n.nikolov@smtm.bg>
 */
enum TimeInterval: string
{
    case FIFTEEN_MINUTE = '15m';
    case THIRTY_MINUTE = '30m';
    case ONE_HOUR = '1h';
    case ONE_DAY = '1d';
}
