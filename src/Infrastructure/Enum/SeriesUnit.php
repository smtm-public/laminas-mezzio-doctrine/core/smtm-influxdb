<?php

namespace Smtm\InfluxDB\Infrastructure\Enum;

/**
 * @author Nikolay Nikolov <n.nikolov@smtm.bg>
 */
enum SeriesUnit: string
{
    case KWH = 'kWh';
    case MWH = 'MWh';
}
