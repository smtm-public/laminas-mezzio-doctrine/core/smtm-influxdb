<?php

namespace Smtm\InfluxDB\Infrastructure\Enum;

/**
 * @author Nikolay Nikolov <n.nikolov@smtm.bg>
 */
enum WritePrecision: string
{
    case S = 's';
    case MS = 'ms';
    case US = 'us';
    case NS = 'ns';
}
