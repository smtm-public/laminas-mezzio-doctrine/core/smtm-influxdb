<?php

namespace Smtm\InfluxDB\Infrastructure\Service\RemoteServiceConnector\Exception;

use Exception;

/**
 * @author Nikolay Nikolov <n.nikolov@smtm.bg>
 */
class InfluxDeleteException extends Exception
{
    public function __construct(string $message = '', int $code = 0, Exception $previous = null)
    {
        if (!$message) {
            $message = 'Influx delete failed due to unknown error';
        }

        parent::__construct($message, $code, $previous);
    }
}
