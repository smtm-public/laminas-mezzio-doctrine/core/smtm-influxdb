<?php

namespace Smtm\InfluxDB\Infrastructure\Service\RemoteServiceConnector\Exception;

use Exception;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class InfluxWriteException extends Exception
{
    public function __construct(string $message = '', int $code = 0, Exception $previous = null)
    {
        if (!$message) {
            $message = 'Influx write failed due to unknown error';
        }

        parent::__construct($message, $code, $previous);
    }
}
