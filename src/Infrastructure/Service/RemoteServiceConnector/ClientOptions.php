<?php

namespace Smtm\InfluxDB\Infrastructure\Service\RemoteServiceConnector;

/**
 * @author Nikolay Nikolov <n.nikolov@smtm.bg>
 */
class ClientOptions
{
    protected \DateTimeZone $timeZone;

    public function __construct(
        protected string $org,
        \DateTimeZone|string $timeZone
    ) {
        if (is_string($timeZone)) {
            $timeZone = new \DateTimeZone($timeZone);
        }

        $this->timeZone = $timeZone;
    }

    public function getOrg(): string
    {
        return $this->org;
    }

    public function getTimeZone(): string
    {
        return $this->timeZone->getName();
    }
}
