<?php

namespace Smtm\InfluxDB\Infrastructure\Service\RemoteServiceConnector;

use GuzzleHttp\Psr7\Utils;
use Psr\Http\Message\RequestInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait InfluxDBRemoteServiceConnectorTrait
{
    protected function setFluxBodyOnRequest(
        RequestInterface $request,
        string $body
    ): RequestInterface {
        $request = $request->withHeader(
            'Content-Type',
            'application/vnd.flux'
        );
        $stream = Utils::streamFor($body);

        return $request->withBody($stream);
    }
}
