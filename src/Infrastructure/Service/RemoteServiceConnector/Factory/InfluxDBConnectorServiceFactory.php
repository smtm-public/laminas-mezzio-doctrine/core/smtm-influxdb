<?php

declare(strict_types=1);

namespace Smtm\InfluxDB\Infrastructure\Service\RemoteServiceConnector\Factory;

use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Smtm\InfluxDB\Infrastructure\Service\RemoteServiceConnector\ClientOptions;
use Smtm\InfluxDB\Infrastructure\Service\RemoteServiceConnector\InfluxDBConnectorService;
use GuzzleHttp\ClientInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class InfluxDBConnectorServiceFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new InfluxDBConnectorService(
            $container->get(InfrastructureServicePluginManager::class),
            $container->get(InfrastructureServicePluginManager::class)->get(ClientInterface::class),
            $options
        );
    }
}
