<?php

namespace Smtm\InfluxDB\Infrastructure\Service\RemoteServiceConnector;

use DateTimeInterface;
use Smtm\Base\Infrastructure\Exception\InvalidArgumentException;
use Smtm\Base\Infrastructure\Helper\ArrayHelper;
use Smtm\Base\Infrastructure\Helper\DateTimeHelper;
use Smtm\Base\Infrastructure\Helper\HttpHelper;
use Smtm\Base\Infrastructure\Service\AbstractRemoteServiceConnector;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Smtm\InfluxDB\Infrastructure\Enum\SeriesUnit;
use Smtm\InfluxDB\Infrastructure\Enum\WritePrecision;
use Smtm\InfluxDB\Infrastructure\ParserOptions;
use Smtm\InfluxDB\Infrastructure\Service\RemoteServiceConnector\Exception\InfluxDeleteException;
use Smtm\InfluxDB\Infrastructure\Service\RemoteServiceConnector\Exception\InfluxReadException;
use Smtm\InfluxDB\Infrastructure\Service\RemoteServiceConnector\Exception\InfluxWriteException;
use Generator;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Psr7\StreamWrapper;
use GuzzleHttp\Psr7\Uri;
use JetBrains\PhpStorm\ArrayShape;
use Psr\Http\Message\ResponseInterface;
use Throwable;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class InfluxDBConnectorService extends AbstractRemoteServiceConnector
{

    use InfluxDBRemoteServiceConnectorTrait;

    protected const TIME_ZONE = 'UTC';
    protected const DATE_TIME_FORMAT = 'Y-m-d\TH:i:s\Z';

    public const BUCKET_SERIES = 'series-bucket';
    public const BUCKET_SITE = 'site-bucket';
    public const BUCKET_TRADE = 'trade-bucket';

    protected ?ClientOptions $clientOptions = null;

    public function __construct(
        InfrastructureServicePluginManager $infrastructureServicePluginManager,
        ClientInterface $client,
        protected ?array $options = null
    ) {
        $this->clientOptions = new ClientOptions(
            $options['clientOptions']['org'] ?? 'default',
            $options['clientOptions']['timeZone'] ?? 'UTC'
        );
        unset($options['clientOptions']);

        parent::__construct($infrastructureServicePluginManager, $client, $options);
    }

    public function ping(): int
    {
        $url = $this->options['scheme'] . '://' . $this->options['host'] . ':' . $this->options['port'] . '/ping';

        $request = $this->createRequest('get', $url);
        $response = $this->sendRequest($request);

        return $response->getStatusCode();
    }

    public function health(): array
    {
        $url = $this->options['scheme'] . '://' . $this->options['host'] . ':' . $this->options['port'] . '/health';

        $request = $this->createRequest('get', $url);
        $response = $this->sendRequest($request);

        return $this->parseResponse($response, HttpHelper::STATUS_CODE_OK);
    }

    public function orgIndex(): array
    {
        $url = $this->options['scheme'] . '://' . $this->options['host'] . ':' . $this->options['port'] . '/api/v2/orgs';

        $request = $this->createRequest('get', $url);

        if ($this->options['authentication']['type'] === 'basic') {
            $request = $this->setAuthenticationBasicOnRequest(
                $request,
                $this->options['authentication']['username'],
                $this->options['authentication']['password']
            );
        } elseif ($this->options['authentication']['type'] === 'token') {
            $request = $this->setAuthenticationTokenOnRequest($request, $this->options['authentication']['token']);
        }

        $response = $this->sendRequest($request);

        return $this->parseResponse($response, HttpHelper::STATUS_CODE_OK);
    }

    public function orgRead(string $id): array
    {
        $url = $this->options['scheme'] . '://' . $this->options['host'] . ':' . $this->options['port'] . '/api/v2/orgs/' . $id;

        $request = $this->createRequest('get', $url);

        if ($this->options['authentication']['type'] === 'basic') {
            $request = $this->setAuthenticationBasicOnRequest(
                $request,
                $this->options['authentication']['username'],
                $this->options['authentication']['password']
            );
        } elseif ($this->options['authentication']['type'] === 'token') {
            $request = $this->setAuthenticationTokenOnRequest($request, $this->options['authentication']['token']);
        }

        $response = $this->sendRequest($request);

        return $this->parseResponse($response, HttpHelper::STATUS_CODE_OK);
    }

    public function bucketIndex(): array
    {
        $url = $this->options['scheme'] . '://' . $this->options['host'] . ':' . $this->options['port'] . '/api/v2/buckets';

        $request = $this->createRequest('get', $url);

        if ($this->options['authentication']['type'] === 'basic') {
            $request = $this->setAuthenticationBasicOnRequest(
                $request,
                $this->options['authentication']['username'],
                $this->options['authentication']['password']
            );
        } elseif ($this->options['authentication']['type'] === 'token') {
            $request = $this->setAuthenticationTokenOnRequest($request, $this->options['authentication']['token']);
        }

        $response = $this->sendRequest($request);

        return $this->parseResponse($response, HttpHelper::STATUS_CODE_OK);
    }

    public function bucketRead(string $id): array
    {
        $url = $this->options['scheme'] . '://' . $this->options['host'] . ':' . $this->options['port'] . '/api/v2/buckets/' . $id;

        $request = $this->createRequest('get', $url);

        if ($this->options['authentication']['type'] === 'basic') {
            $request = $this->setAuthenticationBasicOnRequest(
                $request,
                $this->options['authentication']['username'],
                $this->options['authentication']['password']
            );
        } elseif ($this->options['authentication']['type'] === 'token') {
            $request = $this->setAuthenticationTokenOnRequest($request, $this->options['authentication']['token']);
        }

        $response = $this->sendRequest($request);

        return $this->parseResponse($response, HttpHelper::STATUS_CODE_OK);
    }

    public function query(
        array|string $body,
        ParserOptions|array $parserOptions = [],
        bool $isAsyncRequest = false,
        #[ArrayShape([
            'org' => 'string',
            'orgID' => 'string'
        ])] array $additionalQueryParams = null,
        array $options = [
            self::OPTION_NAME_LOG_EXTRA_DATA => self::LOG_EXTRA_DATA,
        ]
    ): ResponseInterface|array|Generator {
        $queryParams = ['org' => $this->clientOptions->getOrg()];

        if ($additionalQueryParams !== null) {
            $queryParams = array_replace($queryParams, $additionalQueryParams);
        }

        $url = $this->options['scheme'] . '://'
            . $this->options['host']
            . ':' . $this->options['port']
            . '/api/v2/query'
            . '?' . http_build_query($queryParams);
        $request = $this->createRequest('post', $url);

        if ($this->options['authentication']['type'] === 'basic') {
            $request = $this->setAuthenticationBasicOnRequest(
                $request,
                $this->options['authentication']['username'],
                $this->options['authentication']['password']
            );
        } elseif ($this->options['authentication']['type'] === 'token') {
            $request = $this->setAuthenticationTokenOnRequest($request, $this->options['authentication']['token']);
        }

        $body = is_array($body) ? $body : [
            'query' => $body,
            'dialect' => [
                'annotations' => ['group', 'datatype', 'default'],
            ],
        ];
        $request = $this->setJsonBodyOnRequest($request, $body);

        try {
            $response = $this->sendRequest($request, array_merge($options, [self::OPTION_NAME_THROW_ON_ERROR => true]));
        } catch (Throwable $t) {
            $previous = $t->getPrevious();
            $message = $previous->getMessage();

            if ($previous instanceof ServerException || $previous instanceof ClientException) {
                $responseContents = json_decode($previous->getResponse()->getBody()->getContents(), true);

                if (!empty($responseContents)) {
                    $message = sprintf(
                        "Influx retrieve data failed. Details about the error: code '%s' and message '%s'",
                        $responseContents['code'],
                        $responseContents['message']
                    );
                }
            }

            throw new InfluxReadException($message, $previous->getCode(), $t);
        }

        if ($isAsyncRequest) {
            return $response;
        }

        $stream = $response->getBody();
        $iterableResults = $this->csvParser(StreamWrapper::getResource($stream), $parserOptions);

        return
            (
                (is_array($parserOptions) && isset($parserOptions['returnAsIterable']) && $parserOptions['returnAsIterable'])
                || ($parserOptions instanceof ParserOptions && $parserOptions->getReturnAsIterable())
            )
                ? $iterableResults
                : iterator_to_array($iterableResults);
    }

    public function write(
        array|string $body,
        string $bucket,
        ?string $precision = null,
        bool $isAsyncRequest = false,
        #[ArrayShape([
            'org' => 'string',
            'orgID' => 'string'
        ])] array $additionalQueryParams = null,
        array $options = [
            self::OPTION_NAME_LOG_EXTRA_DATA => self::LOG_EXTRA_DATA,
        ]
    ): ResponseInterface {
        $precision ??= WritePrecision::NS->value;

        $queryParams = [
            'bucket' => $bucket,
            'org' => $this->clientOptions->getOrg(),
            'precision' => $precision,
        ];

        if ($additionalQueryParams !== null) {
            $queryParams = array_replace($queryParams, $additionalQueryParams);
        }

        $url = $this->options['scheme'] . '://'
            . $this->options['host']
            . ':' . $this->options['port']
            . '/api/v2/write'
            . '?' . http_build_query($queryParams);

        $request = $this->createRequest('post', $url);

        if ($this->options['authentication']['type'] === 'basic') {
            $request = $this->setAuthenticationBasicOnRequest(
                $request,
                $this->options['authentication']['username'],
                $this->options['authentication']['password']
            );
        } elseif ($this->options['authentication']['type'] === 'token') {
            $request = $this->setAuthenticationTokenOnRequest($request, $this->options['authentication']['token']);
        }

        $body = is_array($body) ? $this->transformToLineProtocol($body, $precision) : $body;
        $request = $this->setTextBodyOnRequest($request, $body);

        try {
            $response = $this->sendRequest($request, array_merge($options, [self::OPTION_NAME_THROW_ON_ERROR => true]));
        } catch (Throwable $t) {
            $previous = $t->getPrevious();
            $message = $previous->getMessage();

            if ($previous instanceof ServerException || $previous instanceof ClientException) {
                $responseContents = json_decode($previous->getResponse()->getBody()->getContents(), true);

                if (!empty($responseContents)) {
                    $message = sprintf(
                        "Influx write failed. Details about the error: code '%s' and message '%s'",
                        $responseContents['code'],
                        $responseContents['message']
                    );
                }
            }

            throw new InfluxWriteException($message, $previous->getCode(), $t);
        }

        return $response;
    }

    public function delete(
        string $bucket,
        #[ArrayShape([
            'start' => DateTimeInterface::class,
            'stop' => DateTimeInterface::class,
            'predicate' => 'string | null'
        ])] array $requestBody,
        #[ArrayShape([
            'org' => 'string',
            'orgID' => 'string'
        ])] array $additionalQueryParams = null,
        array $options = [
            self::OPTION_NAME_LOG_EXTRA_DATA => self::LOG_EXTRA_DATA,
        ]
    ): ResponseInterface {
        if (!isset($requestBody['start']) || !($requestBody['start'] instanceof DateTimeInterface)) {
            throw new InvalidArgumentException(
                'requestBody must contain a "start" and value expected to be instance of DateTimeInterface'
            );
        }

        if (!isset($requestBody['stop']) || !($requestBody['stop'] instanceof DateTimeInterface)) {
            throw new InvalidArgumentException(
                'requestBody must contain a "stop" and value expected to be instance of DateTimeInterface'
            );
        }

        if (isset($requestBody['predicate']) && !is_string($requestBody['predicate'])) {
            throw new InvalidArgumentException('requestBody "predicate" must be string');
        }

        $queryParams = [
            'bucket' => $bucket,
            'org' => $this->clientOptions->getOrg()
        ];
        $bodyData = [
            'start' => $requestBody['start']->format('c'),
            'stop' => $requestBody['stop']->format('c')
        ];

        if ($additionalQueryParams !== null) {
            $queryParams = array_replace($queryParams, $additionalQueryParams);
        }

        if (array_key_exists('predicate', $requestBody)) {
            $bodyData['predicate'] = $requestBody['predicate'];
        }

        $uri = new Uri("{$this->options['scheme']}://{$this->options['host']}:{$this->options['port']}/api/v2/delete");
        $uri = Uri::withQueryValues($uri, $queryParams);
        $request = $this->createRequest('post', $uri);
        $request = $this->setJsonBodyOnRequest($request, $bodyData);

        if ($this->options['authentication']['type'] === 'basic') {
            $request = $this->setAuthenticationBasicOnRequest(
                $request,
                $this->options['authentication']['username'],
                $this->options['authentication']['password']
            );
        } elseif ($this->options['authentication']['type'] === 'token') {
            $request = $this->setAuthenticationTokenOnRequest($request, $this->options['authentication']['token']);
        }

        try {
            $response = $this->sendRequest($request, array_merge($options, [self::OPTION_NAME_THROW_ON_ERROR => true]));
        } catch (\Throwable $t) {
            $previous = $t->getPrevious();
            $message = $previous->getMessage();

            if ($previous instanceof ServerException || $previous instanceof ClientException) {
                $responseContents = json_decode($previous->getResponse()->getBody()->getContents(), true);

                if (!empty($responseContents)) {
                    $message = sprintf(
                        "Influx delete points failed. Details about the error: code '%s' and message '%s'",
                        $responseContents['code'],
                        $responseContents['message']
                    );
                }
            }

            throw new InfluxDeleteException($message, $previous->getCode());
        }

        return $response;
    }

    /**
     * @throws InfluxReadException
     */
    protected function csvParser($resource, ParserOptions|array $options): iterable
    {
        $defaultOptions = [
            //'includedColumns' => ['_time', '_field', '_value'],
            //'castValues' => [],
            'columnCallbacks' => [
                '_time' => [
                    'method' => 'convertDateTime',
                    'params' => [
                        'timezoneTo' => $this->clientOptions->getTimeZone(),
                        'formatTo' => \DateTimeInterface::ATOM
                    ],
                ],
                '_value' => [
                    'fieldKeys' => ['value'],
                    'method' => 'convertValueToUnit',
                    'params' => [
                        'unit' => SeriesUnit::MWH->value
                    ],
                ],
            ],
            'indexOptions' => [
                'column' => null,
                'keepInValues' => false,
            ]
        ];
        $columnsProperties = [];
        $containsErrorMessage = false;
        $headerColumnsIsMapped = false;

        $replaceWithOptions = $options instanceof ParserOptions ? $options->toArray() : $options;
        $options = array_replace_recursive($defaultOptions, $replaceWithOptions);
        $indexOptions = $options['indexOptions'];

        while ($line = stream_get_line($resource, 1024 * 1024, "\n")) {
            $line = trim($line);

            if (empty($line)) {
                continue;
            }

            $fields = str_getcsv($line);

            if ($fields[1] === 'error' && $fields[2] === 'reference') {
                $containsErrorMessage = true;

                continue;
            }

            if ($containsErrorMessage) {
                throw new InfluxReadException(sprintf(
                    "Influx csv parse failed because contains an error. Error details: message '%s' and reference code '%s'",
                    $fields[1],
                    array_key_exists(2, $fields) && !empty($fields[2]) ? $fields[2] : 0
                ));
            }

            if ($fields[0] === '#group') {
                for ($i = 1; $i < count($fields); $i++) {
                    $columnsProperties[$i]['group'] = filter_var($fields[$i], FILTER_VALIDATE_BOOL);
                }
            } elseif ($fields[0] === '#datatype') {
                for ($i = 1; $i < count($fields); $i++) {
                    $columnsProperties[$i]['label'] = null;
                    $columnsProperties[$i]['dataType'] = $fields[$i];
                }
            } elseif ($fields[0] === '#default') {
                for ($i = 1; $i < count($fields); $i++) {
                    $columnsProperties[$i]['default'] = $fields[$i];
                }
            } elseif (!empty($columnsProperties) && !$headerColumnsIsMapped) {
                $headerColumnsIsMapped = true;

                foreach ($columnsProperties as $key => &$column) {
                    $columnLabel = $fields[$key];

                    if (isset($options['includedColumns'])
                        && is_array($options['includedColumns'])
                        && !in_array($columnLabel, $options['includedColumns'])
                    ) {
                        unset($columnsProperties[$key]);
                    }

                    if (ArrayHelper::has($options, "columnCallbacks.$columnLabel.method")) {
                        $column['callback'] = $options['columnCallbacks'][$columnLabel];
                    }

                    $column['label'] = $columnLabel;
                }
            } else {
                $transformedData = $this->transformFields($fields, $columnsProperties, $options);

                if ($indexOptions['column'] && in_array($indexOptions['column'], array_keys($transformedData))) {
                    $key = $transformedData[$indexOptions['column']];

                    if (!$indexOptions['keepInValues']) {
                        unset($transformedData[$indexOptions['column']]);
                    }

                    yield $key => $transformedData;
                } else {
                    yield $transformedData;
                }
            }
        }
    }

    protected function transformFields(
        array $fields,
        array $columnsProperties,
        ?array $options = null
    ): array {
        $options ??= ['castValues' => []];
        $results = [];
        $fieldValue = null;
        $mapColumnsByKeyAndLabel = array_combine(
            array_keys($columnsProperties),
            array_column($columnsProperties, 'label')
        );
        $searchedFieldKey = array_search('_field', $mapColumnsByKeyAndLabel);

        if ($searchedFieldKey) {
            $fieldValue = $fields[$searchedFieldKey];

            if (!array_search('_value', $mapColumnsByKeyAndLabel)) {
                $results['_field'] = $fieldValue;
            }

            unset($columnsProperties[$searchedFieldKey]);
        }

        foreach ($columnsProperties as $index => $columnProperties) {
            $columnLabel = $columnProperties['label'];
            $columnValue = strlen($fields[$index]) > 0 && $fields[$index] !== 'NaN' ? $fields[$index] : null;

            if ($columnValue !== null) {
                if ($columnProperties['dataType'] !== 'string'
                    && (($options['castValues'][$columnLabel] ?? null) !== 'string')
                ) {
                    $columnValue = $this->convertToDataType($columnValue, $columnProperties['dataType']);
                }

                if (array_key_exists('callback', $columnProperties)) {
                    $callbackOptions = $columnProperties['callback'];
                    $callback = [$this, $callbackOptions['method']];

                    if (($columnLabel !== '_value' || ($fieldValue
                                && $columnValue !== 0.0
                                && isset($callbackOptions['fieldKeys'])
                                && is_array($callbackOptions['fieldKeys'])
                                && in_array($fieldValue, $callbackOptions['fieldKeys'])))
                        && is_callable($callback)
                    ) {
                        $callbackParams = ['value' => $columnValue];

                        if (isset($callbackOptions['params']) && is_array($callbackOptions['params'])) {
                            $callbackParams = array_merge($callbackParams, $callbackOptions['params']);
                        }

                        $columnValue = call_user_func_array($callback, $callbackParams);
                    }
                }
            }

            if ($columnLabel === '_value' && $fieldValue) {
                $columnLabel = $fieldValue;
            }

            $results[$columnLabel] = $columnValue;
        }

        return $results;
    }

    protected function convertToDataType(string $value, string $dataType): string|int|float|bool
    {
        return match ($dataType) {
            'boolean' => filter_var($value, FILTER_VALIDATE_BOOLEAN),
            'unsignedLong', 'long' => intval($value),
            'double' => doubleval($value),
            'base64Binary' => base64_decode($value),
            default => $value,
        };
    }

    protected function convertValueToUnit(float $value, ?string $unit = null, ?int $precision = null): float
    {
        $unit ??= SeriesUnit::MWH->value;

        if ($unit === SeriesUnit::MWH->value) {
            $value = $value / 1000;
        }

        return $precision !== null ? $this->roundNumber($value, $precision) : $value;
    }

    protected function convertDateTime(
        string $value,
        ?string $timezoneTo = null,
        ?string $formatTo = null,
        string $timezoneFrom = self::TIME_ZONE
    ): \DateTime|string {
        $dateTime = \DateTime::createFromFormat(
            self::DATE_TIME_FORMAT,
            $value,
            new \DateTimeZone($timezoneFrom)
        );

        if ($timezoneTo) {
            $dateTime->setTimezone(new \DateTimeZone($timezoneTo));
        }

        return $formatTo ? $dateTime->format($formatTo) : $dateTime;
    }

    protected function roundNumber(float|int $value, int $precision = 3): float
    {
        return round($value, $precision);
    }

    /**
     * @throws \InvalidArgumentException|\Exception
     */
    protected function transformToLineProtocol(array $data, string $precision): string
    {
        $lineProtocol = '';

        foreach ($data as $dataPoint) {
            if (!is_array($dataPoint)) {
                throw new \InvalidArgumentException(
                    'Single data point is expected to be an array, not ' . gettype($dataPoint)
                );
            }

            if (!array_key_exists('measurement', $dataPoint) || empty($dataPoint['measurement'])) {
                throw new \InvalidArgumentException(
                    'Single data point must contain a "measurement" and cannot be empty'
                );
            }

            if (!array_key_exists('fields', $dataPoint)
                || !is_array($dataPoint['fields'])
                || empty($dataPoint['fields'])
            ) {
                throw new \InvalidArgumentException(
                    'Single data point must contain a "fields", value expected to be an array and cannot be empty'
                );
            }

            $this->checkBeginWithUnderscore($dataPoint['measurement'], 'Measurement');
            $lineProtocol .= $this->escapeSpecialCharacters($dataPoint['measurement']);

            if (array_key_exists('tags', $dataPoint)) {
                if (!is_array($dataPoint['tags'])) {
                    throw new \InvalidArgumentException(
                        'Tags is expected to be an array, not ' . gettype($dataPoint['tags'])
                    );
                }

                $lineProtocol .= $this->appendTagSet($dataPoint['tags']);
            } else {
                $lineProtocol .= ' ';
            }

            $lineProtocol .= $this->appendFieldSet($dataPoint['fields']);

            if (isset($dataPoint['time'])) {
                $lineProtocol .= $this->appendTime($dataPoint['time'], $precision);
            }

            $lineProtocol .= "\n";
        }

        return rtrim($lineProtocol, "\n");
    }

    /**
     * @throws \Exception
     */
    protected function appendTagSet(array $tags): string
    {
        $tagSet = '';
        $reservedKeys = ['time', 'field', 'measurement'];
        ksort($tags);

        foreach ($tags as $key => $value) {
            if (in_array($key, $reservedKeys)) {
                throw new \Exception('Tag keys cannot be named: ' . implode(", ", $reservedKeys));
            }

            if (empty($key) || empty($value)) {
                throw new \Exception('Tag keys and tag values cannot be empty');
            }

            $tagSet .= ',' . $this->escapeSpecialCharacters($key, 'tagKey')
                . '=' . $this->escapeSpecialCharacters($value, 'tagValue');
        }

        return $tagSet . ' ';
    }

    /**
     * @throws \Exception
     */
    protected function appendFieldSet(array $fields): string
    {
        $fieldSet = '';
        $reservedKey = 'time';
        ksort($fields);

        foreach ($fields as $key => $value) {
            if ($key === $reservedKey || empty($key)) {
                throw new \Exception("Field keys cannot be named '$reservedKey' and cannot be empty");
            }

            if (is_null($value)) {
                throw new \Exception('Field values cannot be null');
            }

            $fieldSet .= $this->escapeSpecialCharacters($key, 'fieldKey')
                . '=' . $this->convertFieldValueToDataType($value) . ',';
        }

        return rtrim($fieldSet, ',');
    }

    protected function appendTime(DateTimeInterface|string|int $time, string $precision): string
    {
        if (is_string($time)) {
            $time = DateTimeHelper::convertDateTime($time, $this->clientOptions->getTimeZone(), 'UTC', 'Y-m-d H:i:s');
        }

        if ($time instanceof DateTimeInterface) {
            $timestamp = $time->getTimestamp();

            $time = match ($precision) {
                WritePrecision::MS->value => $timestamp * 1000,
                WritePrecision::US->value => $timestamp * 1000000,
                WritePrecision::NS->value => $timestamp * 1000000000,
                default => $timestamp,
            };
        }

        return ' ' . $time;
    }

    protected function checkBeginWithUnderscore(string $value, string $element): void
    {
        if (str_starts_with($value, '_')) {
            throw new \InvalidArgumentException(
                "$element cannot begin with an underscore '_'"
            );
        }
    }

    protected function convertFieldValueToDataType(string|float|int|bool $value): string|float
    {
        switch (true) {
            case is_integer($value):
                $convertedValue = $value . 'i';

                break;
            case !preg_match('/^\d+u$/', $value) && is_string($value):
                $convertedValue = '"' . $this->escapeSpecialCharacters($value, 'fieldValue') . '"';

                break;
            case is_bool($value):
                $convertedValue = $value ? 'true' : 'false';

                break;
            default:
                $convertedValue = $value;
        }

        return $convertedValue;
    }

    protected function escapeSpecialCharacters(string $value, ?string $element = null): string
    {
        $specialCharacters = [
            ' ' => '\\ ', ',' => '\\,', "\\" => '\\\\'
        ];

        switch ($element) {
            case 'tagKey':
            case 'tagValue':
            case 'fieldKey':
                $specialCharacters['='] = '\\=';

                break;
            case 'fieldValue':
                $specialCharacters = ['"' => '\\"', "\\" => '\\\\'];

                break;
        }

        return strtr($value, $specialCharacters);
    }
}
