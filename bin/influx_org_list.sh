#!/usr/bin/env bash

set -evx

# Examples:
#
# With SSH:
# vendor/smtm/smtm-influxdb/bin/influx_org_list.sh "smtm-example-app-envname-influxdb-client-container" "http://tick-influxdb:8086" "root" "123.123.123.123"
# bin/influx_org_list.sh "smtm-example-app-envname-influxdb-client-container" "http://tick-influxdb:8086" "root" "123.123.123.123"
#
# Without SSH:
# vendor/smtm/smtm-influxdb/bin/influx_org_list.sh "smtm-example-app-envname-influxdb-client-container" "http://tick-influxdb:8086"
# bin/influx_org_list.sh "smtm-example-app-envname-influxdb-client-container" "http://tick-influxdb:8086"

INFLUX_CLIENT_CONTAINER_NAME="$1"
INFLUX_HOST="$2"
SSH_USER="$3"
SSH_HOST="$4"

echo "Enter InfluxDB token: ";
read -r -s INFLUX_TOKEN;

INFLUX_COMMAND="export INFLUX_TOKEN=${INFLUX_TOKEN}; influx org list --host=${INFLUX_HOST} --token=${INFLUX_TOKEN}"
DOCKER_EXEC_COMMAND="docker exec -i ${INFLUX_CLIENT_CONTAINER_NAME} bash -c "${INFLUX_COMMAND@Q}
SSH_COMMAND="${DOCKER_EXEC_COMMAND}"
if [[ -n ${SSH_HOST} ]]
then
    SSH_COMMAND="ssh -n -t ${SSH_USER}@${SSH_HOST} "${DOCKER_EXEC_COMMAND@Q}
fi
eval "${SSH_COMMAND}"
