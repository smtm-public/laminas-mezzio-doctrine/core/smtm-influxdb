#!/usr/bin/env bash

set -evx

# Examples:
#
# With SSH:
# vendor/smtm/smtm-influxdb/bin/influx_bucket_list.sh "smtm-example-app-envname-influxdb-client-container" "http://tick-influxdb:8086" "root" "123.123.123.123"
# bin/influx_bucket_list.sh "smtm-example-app-envname-influxdb-client-container" "http://tick-influxdb:8086" "root" "123.123.123.123"
#
# Without SSH:
# vendor/smtm/smtm-influxdb/bin/influx_bucket_list.sh "smtm-example-app-envname-influxdb-client-container" "http://tick-influxdb:8086"
# bin/influx_bucket_list.sh "smtm-example-app-envname-influxdb-client-container" "http://tick-influxdb:8086"

INFLUX_CLIENT_CONTAINER_NAME="$1"
INFLUX_HOST="$2"
SSH_USER="$3"
SSH_HOST="$4"

echo "Enter InfluxDB token: ";
read -r -s INFLUX_TOKEN;

echo "Enter InfluxDB org [leave empty to be prompted for orgID]: ";
read -r INFLUX_ORG;

INFLUX_ORG_OPTION=""
if [[ -z "${INFLUX_ORG}" ]]
then
    echo "Enter InfluxDB orgID: ";
    read -r INFLUX_ORG_ID;
    INFLUX_ORG_OPTION="--org-id="'"'"${INFLUX_ORG_ID}"'"';
    if [[ -z "${SSH_HOST}" ]]
    then
        INFLUX_ORG=$(docker exec -it "${INFLUX_CLIENT_CONTAINER_NAME}" bash -c 'export INFLUX_TOKEN="'"${INFLUX_TOKEN}"'"; influx org list --host="'"${INFLUX_HOST}"'"' | grep "\\b${INFLUX_ORG_ID}\\b" | awk '{print $2}')
    else
        INFLUX_ORG=$(ssh -t "${SSH_USER}@${SSH_HOST}" ${SSH_PRIVATE_KEY_OPTION} "docker exec -it ${INFLUX_CLIENT_CONTAINER_NAME} bash -c 'export INFLUX_TOKEN="'"'"${INFLUX_TOKEN}"'"'"; influx org list --host="'"'"${INFLUX_HOST}"'"'"'" | grep "\\b${INFLUX_ORG_ID}\\b" | awk '{print $2}')
    fi
else
    INFLUX_ORG_OPTION="--org="'"'"${INFLUX_ORG}"'"';
    if [[ -z "${SSH_HOST}" ]]
    then
        INFLUX_ORG_ID=$(docker exec -it "${INFLUX_CLIENT_CONTAINER_NAME}" bash -c 'export INFLUX_TOKEN="'"${INFLUX_TOKEN}"'"; influx org list --host="'"${INFLUX_HOST}"'"' | grep "\\b${INFLUX_ORG}\\b" | awk '{print $1}')
    else
        INFLUX_ORG_ID=$(ssh -t "${SSH_USER}@${SSH_HOST}" ${SSH_PRIVATE_KEY_OPTION} "docker exec -it ${INFLUX_CLIENT_CONTAINER_NAME} bash -c 'export INFLUX_TOKEN="'"'"${INFLUX_TOKEN}"'"'"; influx org list --host="'"'"${INFLUX_HOST}"'"'"'" | grep "\\b${INFLUX_ORG}\\b" | awk '{print $1}')
    fi
fi

if [[ -z "${INFLUX_ORG}" || -z "${INFLUX_ORG_ID}" ]]
then
    echo 'No org or orgID has been specified. Exiting...';
    exit 1;
fi

DOCKER_EXEC_COMMAND="docker exec -it ${INFLUX_CLIENT_CONTAINER_NAME} bash -c 'export INFLUX_TOKEN="'"'"${INFLUX_TOKEN}"'"'"; influx bucket list --host="'"'"${INFLUX_HOST}"'"'" ${INFLUX_ORG_OPTION}'"
COMMAND="${DOCKER_EXEC_COMMAND}"
if [[ -n ${SSH_HOST} ]]
then
    COMMAND='ssh -t "'"${SSH_USER}@${SSH_HOST}"'" "'"${DOCKER_EXEC_COMMAND}"'"'
fi
eval "${COMMAND}"
