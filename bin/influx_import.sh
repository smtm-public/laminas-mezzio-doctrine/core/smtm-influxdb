#!/usr/bin/env bash

set -evx

# Examples:
#
# With SSH:
# vendor/smtm/smtm-influxdb/bin/influx_import.sh "adex-example-app-influxdb-client-container" "123.123.123.123.smtm-example-app-influxdb-container.orgID.org.bucketID.bucket.2023-10-20T00:00:00.completed2023-10-20T00:10:00" "http://tick-influxdb:8086"
# bin/influx_import.sh "adex-example-app-influxdb-client-container" "123.123.123.123.smtm-example-app-influxdb-container.orgID.org.bucketID.bucket.2023-10-20T00:00:00.completed2023-10-20T00:10:00" "http://tick-influxdb:8086"
#
# Without SSH:
# vendor/smtm/smtm-influxdb/bin/influx_import.sh "adex-example-app-influxdb-client-container" "smtm-example-app-influxdb-container.orgID.org.bucketID.bucket.2023-10-20T00:00:00.completed2023-10-20T00:10:00" "http://tick-influxdb:8086"
# bin/influx_import.sh "adex-example-app-influxdb-client-container" "smtm-example-app-influxdb-container.orgID.org.bucketID.bucket.2023-10-20T00:00:00.completed2023-10-20T00:10:00" "http://tick-influxdb:8086"

INFLUX_CLIENT_CONTAINER_NAME="$1"
INFLUX_LP_DUMP_FILE_PATH="$2"
INFLUX_HOST="$3"
SSH_USER="$4"
SSH_HOST="$5"

SSH_PRIVATE_KEY_OPTION=""
if [[ -n "${SSH_HOST}" ]]
then
    echo "Enter SSH private key path [leave empty to use password]: ";
    read -r SSH_PRIVATE_KEY_PATH;
    if [[ -n "${SSH_PRIVATE_KEY_PATH}" ]]
    then
        SSH_PRIVATE_KEY_OPTION="-i ${SSH_PRIVATE_KEY_PATH}";
    fi
fi

echo "Enter InfluxDB token: ";
read -r -s INFLUX_TOKEN;

echo "Enter InfluxDB org [leave empty to be prompted for orgID]: ";
read -r INFLUX_ORG;

INFLUX_ORG_OPTION=""
if [[ -z "${INFLUX_ORG}" ]]
then
    echo "Enter InfluxDB orgID: ";
    read -r INFLUX_ORG_ID;
    INFLUX_ORG_OPTION="--org-id="'"'"${INFLUX_ORG_ID}"'"';
    if [[ -z "${SSH_HOST}" ]]
    then
        INFLUX_ORG=$(docker exec -it "${INFLUX_CLIENT_CONTAINER_NAME}" bash -c 'export INFLUX_TOKEN="'"${INFLUX_TOKEN}"'"; influx org list --host="'"${INFLUX_HOST}"'"' | grep "\\b${INFLUX_ORG_ID}\\b" | awk '{print $2}')
    else
        INFLUX_ORG=$(ssh -t "${SSH_USER}@${SSH_HOST}" ${SSH_PRIVATE_KEY_OPTION} "docker exec -it ${INFLUX_CLIENT_CONTAINER_NAME} bash -c 'export INFLUX_TOKEN="'"'"${INFLUX_TOKEN}"'"'"; influx org list --host="'"'"${INFLUX_HOST}"'"'"'" | grep "\\b${INFLUX_ORG_ID}\\b" | awk '{print $2}')
    fi
else
    INFLUX_ORG_OPTION="--org="'"'"${INFLUX_ORG}"'"';
    if [[ -z "${SSH_HOST}" ]]
    then
        INFLUX_ORG_ID=$(docker exec -it "${INFLUX_CLIENT_CONTAINER_NAME}" bash -c 'export INFLUX_TOKEN="'"${INFLUX_TOKEN}"'"; influx org list --host="'"${INFLUX_HOST}"'"' | grep "\\b${INFLUX_ORG}\\b" | awk '{print $1}')
    else
        INFLUX_ORG_ID=$(ssh -t "${SSH_USER}@${SSH_HOST}" ${SSH_PRIVATE_KEY_OPTION} "docker exec -it ${INFLUX_CLIENT_CONTAINER_NAME} bash -c 'export INFLUX_TOKEN="'"'"${INFLUX_TOKEN}"'"'"; influx org list --host="'"'"${INFLUX_HOST}"'"'"'" | grep "\\b${INFLUX_ORG}\\b" | awk '{print $1}')
    fi
fi

if [[ -z "${INFLUX_ORG}" || -z "${INFLUX_ORG_ID}" ]]
then
    echo 'No org or orgID has been specified. Exiting...';
    exit 1;
fi

echo "Enter InfluxDB bucket [leave empty to be prompted for bucketID]: ";
read -r INFLUX_BUCKET;

INFLUX_BUCKET_OPTION=""
if [[ -z "${INFLUX_BUCKET}" ]]
then
    echo "Enter InfluxDB bucketID: ";
    read -r INFLUX_BUCKET_ID;
    INFLUX_BUCKET_OPTION="--bucket-id="'"'"${INFLUX_BUCKET_ID}"'"';
    if [[ -z "${SSH_HOST}" ]]
    then
        INFLUX_BUCKET=$(docker exec -it "${INFLUX_CLIENT_CONTAINER_NAME}" bash -c 'export INFLUX_TOKEN="'"${INFLUX_TOKEN}"'"; influx bucket list --host="'"${INFLUX_HOST}"'" --org-id="'"${INFLUX_ORG_ID}"'"' | grep "\\b${INFLUX_BUCKET_ID}\\b" | awk '{print $2}')
    else
        INFLUX_BUCKET=$(ssh -t "${SSH_USER}@${SSH_HOST}" ${SSH_PRIVATE_KEY_OPTION} "docker exec -it ${INFLUX_CLIENT_CONTAINER_NAME} bash -c 'export INFLUX_TOKEN="'"'"${INFLUX_TOKEN}"'"'"; influx bucket list --host="'"'"${INFLUX_HOST}"'"'" --org-id="'"'"${INFLUX_ORG_ID}"'"'"'" | grep "\\b${INFLUX_BUCKET_ID}\\b" | awk '{print $2}')
    fi
else
    INFLUX_BUCKET_OPTION="--bucket="'"'"${INFLUX_BUCKET}"'"';
    if [[ -z "${SSH_HOST}" ]]
    then
        INFLUX_BUCKET_ID=$(docker exec -it "${INFLUX_CLIENT_CONTAINER_NAME}" bash -c 'export INFLUX_TOKEN="'"${INFLUX_TOKEN}"'"; influx bucket list --host="'"${INFLUX_HOST}"'" --org-id="'"${INFLUX_ORG_ID}"'"' | grep "\\b${INFLUX_BUCKET}\\b" | awk '{print $1}')
    else
        INFLUX_BUCKET_ID=$(ssh -t "${SSH_USER}@${SSH_HOST}" ${SSH_PRIVATE_KEY_OPTION} "docker exec -it ${INFLUX_CLIENT_CONTAINER_NAME} bash -c 'export INFLUX_TOKEN="'"'"${INFLUX_TOKEN}"'"'"; influx bucket list --host="'"'"${INFLUX_HOST}"'"'" --org-id="'"'"${INFLUX_ORG_ID}"'"'"'" | grep "\\b${INFLUX_BUCKET}\\b" | awk '{print $1}')
    fi
fi

if [[ -z "${INFLUX_BUCKET}" || -z "${INFLUX_BUCKET_ID}" ]]
then
    echo 'No bucket or bucketID has been specified. Exiting...';
    exit 1;
fi

read -r -p "WARNING: Data from file ${INFLUX_LP_DUMP_FILE_PATH} will be imported into host ${INFLUX_HOST}, orgID ${INFLUX_ORG_ID}, org ${INFLUX_ORG}, bucketID ${INFLUX_BUCKET_ID}, bucket ${INFLUX_BUCKET}!!! (Answer with yes if you would like to continue) " yn

case $yn in
    yes ) echo 'Importing...';;
    * ) echo 'Exiting...';
      exit;;
esac

BASH_NESTED_COMMAND="export INFLUX_TOKEN=${INFLUX_TOKEN}; influx write --file=${INFLUX_LP_DUMP_FILE_PATH} --host=${INFLUX_HOST} ${INFLUX_ORG_OPTION} ${INFLUX_BUCKET_OPTION}"
BASH_NESTED_COMMAND="influx write --host=${INFLUX_HOST} --token=${INFLUX_TOKEN} ${INFLUX_ORG_OPTION} ${INFLUX_BUCKET_OPTION}"
DOCKER_EXEC_COMMAND="cat ${INFLUX_LP_DUMP_FILE_PATH} | docker exec -i ${INFLUX_CLIENT_CONTAINER_NAME} bash -c "${BASH_NESTED_COMMAND@Q}
COMMAND="${DOCKER_EXEC_COMMAND}"
if [[ -n ${SSH_HOST} ]]
then
    BASH_NESTED_COMMAND="influx write --host=${INFLUX_HOST} --token=${INFLUX_TOKEN} ${INFLUX_ORG_OPTION} ${INFLUX_BUCKET_OPTION}"
    DOCKER_EXEC_COMMAND="docker exec -i ${INFLUX_CLIENT_CONTAINER_NAME} bash -c "${BASH_NESTED_COMMAND@Q}
    #DOCKER_EXEC_COMMAND="docker exec -it ${INFLUX_CLIENT_CONTAINER_NAME} cat"
    COMMAND="cat ${INFLUX_LP_DUMP_FILE_PATH} | ssh ${SSH_USER}@${SSH_HOST} ${SSH_PRIVATE_KEY_OPTION} "${DOCKER_EXEC_COMMAND@Q}
fi
eval "${COMMAND}"
