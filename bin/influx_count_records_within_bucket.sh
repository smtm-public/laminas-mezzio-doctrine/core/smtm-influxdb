#!/usr/bin/env bash

set -evx

# Examples:
#
# With SSH:
# vendor/smtm/smtm-influxdb/bin/influx_count_records_within_bucket.sh "adex-example-app-influxdb-client-container" "http://tick-influxdb:8086" "root" "123.123.123.123"
# bin/influx_count_records_within_bucket.sh "adex-example-app-influxdb-client-container" "http://tick-influxdb:8086" "root" "123.123.123.123"
#
# Without SSH:
# vendor/smtm/smtm-influxdb/bin/influx_count_records_within_bucket.sh "adex-example-app-influxdb-client-container" "http://tick-influxdb:8086"
# bin/influx_count_records_within_bucket.sh "adex-example-app-influxdb-client-container" "http://tick-influxdb:8086"

INFLUX_CLIENT_CONTAINER_NAME="$1"
INFLUX_HOST="$2"
SSH_USER="$3"
SSH_HOST="$4"

SSH_PRIVATE_KEY_OPTION=""
if [[ -n "${SSH_HOST}" ]]
then
    echo "Enter SSH private key path [leave empty to use password]: ";
    read -r SSH_PRIVATE_KEY_PATH;
    if [[ ! -z "${SSH_PRIVATE_KEY_PATH}" ]]
    then
        SSH_PRIVATE_KEY_OPTION="-i ${SSH_PRIVATE_KEY_PATH}";
    fi
fi

echo "Enter InfluxDB token: ";
read -r -s INFLUX_TOKEN;

echo "Enter InfluxDB org [leave empty to be prompted for orgID]: ";
read -r INFLUX_ORG;

INFLUX_ORG_OPTION=""
if [[ -z "${INFLUX_ORG}" ]]
then
    echo "Enter InfluxDB orgID: ";
    read -r INFLUX_ORG_ID;
    INFLUX_ORG_OPTION="--org-id="'"'"${INFLUX_ORG_ID}"'"';
    if [[ -z "${SSH_HOST}" ]]
    then
        INFLUX_ORG=$(docker exec -it "${INFLUX_CLIENT_CONTAINER_NAME}" bash -c 'export INFLUX_TOKEN="'"${INFLUX_TOKEN}"'"; influx org list --host="'"${INFLUX_HOST}"'"' | grep "\\b${INFLUX_ORG_ID}\\b" | awk '{print $2}')
    else
        INFLUX_ORG=$(ssh -t "${SSH_USER}@${SSH_HOST}" ${SSH_PRIVATE_KEY_OPTION} "docker exec -it ${INFLUX_CLIENT_CONTAINER_NAME} bash -c 'export INFLUX_TOKEN="'"'"${INFLUX_TOKEN}"'"'"; influx org list --host="'"'"${INFLUX_HOST}"'"'"'" | grep "\\b${INFLUX_ORG_ID}\\b" | awk '{print $2}')
    fi
else
    INFLUX_ORG_OPTION="--org="'"'"${INFLUX_ORG}"'"';
    if [[ -z "${SSH_HOST}" ]]
    then
        INFLUX_ORG_ID=$(docker exec -it "${INFLUX_CLIENT_CONTAINER_NAME}" bash -c 'export INFLUX_TOKEN="'"${INFLUX_TOKEN}"'"; influx org list --host="'"${INFLUX_HOST}"'"' | grep "\\b${INFLUX_ORG}\\b" | awk '{print $1}')
    else
        INFLUX_ORG_ID=$(ssh -t "${SSH_USER}@${SSH_HOST}" ${SSH_PRIVATE_KEY_OPTION} "docker exec -it ${INFLUX_CLIENT_CONTAINER_NAME} bash -c 'export INFLUX_TOKEN="'"'"${INFLUX_TOKEN}"'"'"; influx org list --host="'"'"${INFLUX_HOST}"'"'"'" | grep "\\b${INFLUX_ORG}\\b" | awk '{print $1}')
    fi
fi

if [[ -z "${INFLUX_ORG}" || -z "${INFLUX_ORG_ID}" ]]
then
    echo 'No org or orgID has been specified. Exiting...';
    exit 1;
fi

echo "Enter InfluxDB bucket [leave empty to be prompted for bucketID]: ";
read -r INFLUX_BUCKET;

INFLUX_BUCKET_OPTION=""
if [[ -z "${INFLUX_BUCKET}" ]]
then
    echo "Enter InfluxDB bucketID: ";
    read -r INFLUX_BUCKET_ID;
    INFLUX_BUCKET_OPTION="--bucket-id="'"'"${INFLUX_BUCKET_ID}"'"';
    if [[ -z "${SSH_HOST}" ]]
    then
        INFLUX_BUCKET=$(docker exec -it "${INFLUX_CLIENT_CONTAINER_NAME}" bash -c 'export INFLUX_TOKEN="'"${INFLUX_TOKEN}"'"; influx bucket list --host="'"${INFLUX_HOST}"'" --org-id="'"${INFLUX_ORG_ID}"'"' | grep "\\b${INFLUX_BUCKET_ID}\\b" | awk '{print $2}')
    else
        INFLUX_BUCKET=$(ssh -t "${SSH_USER}@${SSH_HOST}" ${SSH_PRIVATE_KEY_OPTION} "docker exec -it ${INFLUX_CLIENT_CONTAINER_NAME} bash -c 'export INFLUX_TOKEN="'"'"${INFLUX_TOKEN}"'"'"; influx bucket list --host="'"'"${INFLUX_HOST}"'"'" --org-id="'"'"${INFLUX_ORG_ID}"'"'"'" | grep "\\b${INFLUX_BUCKET_ID}\\b" | awk '{print $2}')
    fi
else
    INFLUX_BUCKET_OPTION="--bucket="'"'"${INFLUX_BUCKET}"'"';
    if [[ -z "${SSH_HOST}" ]]
    then
        INFLUX_BUCKET_ID=$(docker exec -it "${INFLUX_CLIENT_CONTAINER_NAME}" bash -c 'export INFLUX_TOKEN="'"${INFLUX_TOKEN}"'"; influx bucket list --host="'"${INFLUX_HOST}"'" --org-id="'"${INFLUX_ORG_ID}"'"' | grep "\\b${INFLUX_BUCKET}\\b" | awk '{print $1}')
    else
        INFLUX_BUCKET_ID=$(ssh -t "${SSH_USER}@${SSH_HOST}" ${SSH_PRIVATE_KEY_OPTION} "docker exec -it ${INFLUX_CLIENT_CONTAINER_NAME} bash -c 'export INFLUX_TOKEN="'"'"${INFLUX_TOKEN}"'"'"; influx bucket list --host="'"'"${INFLUX_HOST}"'"'" --org-id="'"'"${INFLUX_ORG_ID}"'"'"'" | grep "\\b${INFLUX_BUCKET}\\b" | awk '{print $1}')
    fi
fi

if [[ -z "${INFLUX_BUCKET}" || -z "${INFLUX_BUCKET_ID}" ]]
then
    echo 'No bucket or bucketID has been specified. Exiting...';
    exit 1;
fi

INFLUX_RANGE_START="1970-01-01T00:00:00Z";
read -r -p "Do you want to use a start time for the range, which is different from the default (1970-01-01T00:00:00Z)? (Answer with yes to be prompted to enter a custom value) " yn
case $yn in
    yes ) echo "Enter start time for the range: "; read -r INFLUX_RANGE_START;;
esac
INFLUX_QUERY_RANGE_START="start: ${INFLUX_RANGE_START}";
if [[ -z "${INFLUX_RANGE_START}" ]]
then
    echo 'Start time for the range cannot be empty. Exiting...';
    exit 1;
fi

INFLUX_RANGE_STOP="$(date --utc +"%Y-%m-%dT%H:%M:%SZ" -d "+1 year")";
read -r -p "Do you want to use a stop time for the range, which is different from the default (+1 year from now)? (Answer with yes to be prompted to enter a custom value) " yn
case $yn in
    yes ) echo "Enter stop time for the range [leave empty to be use only the start value]: "; read -r INFLUX_RANGE_STOP;;
esac
if [[ -z "${INFLUX_RANGE_STOP}" ]]
then
    INFLUX_QUERY_RANGE_STOP="";
else
    INFLUX_QUERY_RANGE_STOP=", stop: ${INFLUX_RANGE_STOP}";
fi

INFLUX_QUERY="from(bucket: "'\"'"${INFLUX_BUCKET}"'\"'") |> range(${INFLUX_QUERY_RANGE_START}${INFLUX_QUERY_RANGE_STOP}) |> keep(columns:["'\"'"_time"'\"'"]) |> map(fn: (r) => ({ r with _value: 0})) |> group() |> count()";

DOCKER_EXEC_COMMAND="docker exec -it ${INFLUX_CLIENT_CONTAINER_NAME} bash -c 'export INFLUX_TOKEN="'"'"${INFLUX_TOKEN}"'"'"; echo "'"'"${INFLUX_QUERY}"'"'" | influx query --host="'"'"${INFLUX_HOST}"'"'" ${INFLUX_ORG_OPTION} -'"
COMMAND="${DOCKER_EXEC_COMMAND}"
if [[ ! -z ${SSH_HOST} ]]
then
    COMMAND='ssh -t "'"${SSH_USER}@${SSH_HOST}"'" '${SSH_PRIVATE_KEY_OPTION}' "'"${DOCKER_EXEC_COMMAND}"'"'
fi
eval "${COMMAND}"
