#!/usr/bin/env bash

set -evx

# Examples:
#
# With SSH:
# vendor/smtm/smtm-influxdb/bin/influx_backup.sh "smtm-example-app-envname-influxdb-container" "/var/lib/influxdbv2/engine/" "root" "123.123.123.123"
# bin/influx_backup.sh "smtm-example-app-envname-influxdb-container" "/var/lib/influxdbv2/engine/" "root" "123.123.123.123"
#
# Without SSH:
# vendor/smtm/smtm-influxdb/bin/influx_backup.sh "smtm-example-app-envname-influxdb-container" "/var/lib/influxdbv2/engine/"
# bin/influx_backup.sh "smtm-example-app-envname-influxdb-container" "/var/lib/influxdbv2/engine/"

INFLUX_CONTAINER_NAME="$1"
INFLUX_ENGINE_DATA_FILES_PATH="$2"
SSH_USER="$3"
SSH_HOST="$4"

SSH_PRIVATE_KEY_OPTION=""
if [[ -n "${SSH_HOST}" ]]
then
    echo "Enter SSH private key path [leave empty to use password]: ";
    read -r SSH_PRIVATE_KEY_PATH;
    if [[ -n "${SSH_PRIVATE_KEY_PATH}" ]]
    then
        SSH_PRIVATE_KEY_OPTION="-i ${SSH_PRIVATE_KEY_PATH}";
    fi
fi

echo "Enter InfluxDB token: ";
read -r -s INFLUX_TOKEN;

INFLUX_HOST="http://127.0.0.1:8086";

echo "Enter InfluxDB org [leave empty to be prompted for orgID]: ";
read -r INFLUX_ORG;

INFLUX_ORG_OPTION=""
if [[ -z "${INFLUX_ORG}" ]]
then
    echo "Enter InfluxDB orgID: ";
    read -r INFLUX_ORG_ID;
    INFLUX_ORG_OPTION="--org-id="'"'"${INFLUX_ORG_ID}"'"';
    if [[ -z "${SSH_HOST}" ]]
    then
        INFLUX_ORG=$(docker exec -it ${INFLUX_CONTAINER_NAME} bash -c 'export INFLUX_TOKEN="'"${INFLUX_TOKEN}"'"; influx org list --host="'"${INFLUX_HOST}"'"' | grep "\\b${INFLUX_ORG_ID}\\b" | awk '{print $2}')
    else
        INFLUX_ORG=$(ssh -t "${SSH_USER}@${SSH_HOST}" ${SSH_PRIVATE_KEY_OPTION} "docker exec -it ${INFLUX_CONTAINER_NAME} bash -c 'export INFLUX_TOKEN="'"'"${INFLUX_TOKEN}"'"'"; influx org list --host="'"'"${INFLUX_HOST}"'"'"'" | grep "\\b${INFLUX_ORG_ID}\\b" | awk '{print $2}')
    fi
else
    INFLUX_ORG_OPTION="--org="'"'"${INFLUX_ORG}"'"';
    if [[ -z "${SSH_HOST}" ]]
    then
        INFLUX_ORG_ID=$(docker exec -it ${INFLUX_CONTAINER_NAME} bash -c 'export INFLUX_TOKEN="'"${INFLUX_TOKEN}"'"; influx org list --host="'"${INFLUX_HOST}"'"' | grep "\\b${INFLUX_ORG}\\b" | awk '{print $1}')
    else
        INFLUX_ORG_ID=$(ssh -t "${SSH_USER}@${SSH_HOST}" ${SSH_PRIVATE_KEY_OPTION} "docker exec -it ${INFLUX_CONTAINER_NAME} bash -c 'export INFLUX_TOKEN="'"'"${INFLUX_TOKEN}"'"'"; influx org list --host="'"'"${INFLUX_HOST}"'"'"'" | grep "\\b${INFLUX_ORG}\\b" | awk '{print $1}')
    fi
fi

if [[ -z "${INFLUX_ORG}" || -z "${INFLUX_ORG_ID}" ]]
then
    echo 'No org or orgID has been specified. Exiting...';
    exit 1;
fi

echo "Enter InfluxDB bucket [leave empty to be prompted for bucketID]: ";
read -r INFLUX_BUCKET;

INFLUX_BUCKET_OPTION=""
if [[ -z "${INFLUX_BUCKET}" ]]
then
    echo "Enter InfluxDB bucketID: ";
    read -r INFLUX_BUCKET_ID;
    INFLUX_BUCKET_OPTION="--bucket-id="'"'"${INFLUX_BUCKET_ID}"'"';
    if [[ -z "${SSH_HOST}" ]]
    then
        INFLUX_BUCKET=$(docker exec -it "${INFLUX_CONTAINER_NAME}" bash -c 'export INFLUX_TOKEN="'"${INFLUX_TOKEN}"'"; influx bucket list --host="'"${INFLUX_HOST}"'" --org-id="'"${INFLUX_ORG_ID}"'"' | grep "\\b${INFLUX_BUCKET_ID}\\b" | awk '{print $2}')
    else
        INFLUX_BUCKET=$(ssh -t "${SSH_USER}@${SSH_HOST}" ${SSH_PRIVATE_KEY_OPTION} "docker exec -it ${INFLUX_CONTAINER_NAME} bash -c 'export INFLUX_TOKEN="'"'"${INFLUX_TOKEN}"'"'"; influx bucket list --host="'"'"${INFLUX_HOST}"'"'" --org-id="'"'"${INFLUX_ORG_ID}"'"'"'" | grep "\\b${INFLUX_BUCKET_ID}\\b" | awk '{print $2}')
    fi
else
    INFLUX_BUCKET_OPTION="--bucket="'"'"${INFLUX_BUCKET}"'"';
    if [[ -z "${SSH_HOST}" ]]
    then
        INFLUX_BUCKET_ID=$(docker exec -it ${INFLUX_CONTAINER_NAME} bash -c 'export INFLUX_TOKEN="'"${INFLUX_TOKEN}"'"; influx bucket list --host="'"${INFLUX_HOST}"'" --org-id="'"${INFLUX_ORG_ID}"'"' | grep "\\b${INFLUX_BUCKET}\\b" | awk '{print $1}')
    else
        INFLUX_BUCKET_ID=$(ssh -t "${SSH_USER}@${SSH_HOST}" ${SSH_PRIVATE_KEY_OPTION} "docker exec -it ${INFLUX_CONTAINER_NAME} bash -c 'export INFLUX_TOKEN="'"'"${INFLUX_TOKEN}"'"'"; influx bucket list --host="'"'"${INFLUX_HOST}"'"'" --org-id="'"'"${INFLUX_ORG_ID}"'"'"'" | grep "\\b${INFLUX_BUCKET}\\b" | awk '{print $1}')
    fi
fi

if [[ -z "${INFLUX_BUCKET}" || -z "${INFLUX_BUCKET_ID}" ]]
then
    echo 'No bucket or bucketID has been specified. Exiting...';
    exit 1;
fi

export INFLUX_DUMP_DATE_INITIATED="$(date +%Y-%m-%dT%H-%M-%S)"
INFLUX_LP_DUMP_FILE_NAME_INITIATED="${INFLUX_CONTAINER_NAME}.${INFLUX_ORG_ID}.${INFLUX_ORG}.${INFLUX_BUCKET_ID}.${INFLUX_BUCKET}.${INFLUX_DUMP_DATE_INITIATED}"
if [[ -n ${SSH_HOST} ]]
then
    INFLUX_LP_DUMP_FILE_NAME_INITIATED="${SSH_HOST}.${INFLUX_CONTAINER_NAME}.${INFLUX_ORG_ID}.${INFLUX_ORG}.${INFLUX_BUCKET_ID}.${INFLUX_BUCKET}.${INFLUX_DUMP_DATE_INITIATED}"
fi

echo "Enter start timestamp [empty to skip]: ";
read -r INFLUX_START;

if [ -n "${INFLUX_START}" ]
then
    INFLUX_START_OPTION="--start="'"'"${INFLUX_START}"'"';
fi

echo "Enter end timestamp [empty to skip]: ";
read -r INFLUX_END;

if [ -n "${INFLUX_END}" ]
then
    INFLUX_END_OPTION="--end="'"'"${INFLUX_END}"'"';
fi

DOCKER_EXEC_COMMAND="docker exec -it ${INFLUX_CONTAINER_NAME} bash -c 'influxd inspect export-lp --log-level=error --engine-path="'"'"${INFLUX_ENGINE_DATA_FILES_PATH}"'"'" --bucket-id="'"'"${INFLUX_BUCKET_ID}"'"'" ${INFLUX_START_OPTION} ${INFLUX_END_OPTION} --output-path=-'"
COMMAND="${DOCKER_EXEC_COMMAND}"' 2>/dev/null 1>'"${INFLUX_LP_DUMP_FILE_NAME_INITIATED}"
if [[ -n ${SSH_HOST} ]]
then
    COMMAND='ssh -t "'"${SSH_USER}@${SSH_HOST}"'" '${SSH_PRIVATE_KEY_OPTION}' "'"${DOCKER_EXEC_COMMAND}"'" 2>/dev/null 1>'"${INFLUX_LP_DUMP_FILE_NAME_INITIATED}"
fi
eval "${COMMAND}"
export INFLUX_DUMP_DATE_COMPLETED="$(date +%Y-%m-%dT%H-%M-%S)"
INFLUX_LP_DUMP_FILE_NAME_COMPLETED="${INFLUX_LP_DUMP_FILE_NAME_INITIATED}.completed${INFLUX_DUMP_DATE_COMPLETED}"
mv "${INFLUX_LP_DUMP_FILE_NAME_INITIATED}" "${INFLUX_LP_DUMP_FILE_NAME_COMPLETED}.lp"

echo "${INFLUX_LP_DUMP_FILE_NAME_COMPLETED}.lp"
